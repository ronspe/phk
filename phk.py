# -*- coding: utf-8 -*-
"""
This will start Python HotKey (PHK).
It will load the user scripts and starts listening to the keyboard
for hotkeys and phrases.
End with: Ctrl-c
"""
import os
import time
import pickle
import app
from app.config.ini import Ini, fallback_phrases, fallback_hotkeys, fallback_dtm
from app.phk.hotkey import Hotkey
from app.phk.phrase import Phrase
from app.script.load_scripts import LoadScripts
from app.phk.keyboard_listener import KeyboardListener
from app.run.args import get_args

import app.config as config
log = config.default_logger


class Phk:

    def __init__(self) -> None:
        self.kl = KeyboardListener()

    def start(self, phk_version: str, pid: int) -> None:
        """
        Load the user scripts into the listener and start listening
        to the keyboard.
        """
        try:
            log.debug("===================== STARTING PHK ===================== ")
            this_dir = os.path.dirname(os.path.realpath(__file__))
            user_script_path = os.path.join(this_dir, app.user.root_folder)
            LoadScripts(self.kl, user_script_path, app.user.context_folder)
            self._write_key_reference_file()
            self.pickle_files()
            # Activate the listener
            print(f"PHK started. Version: {phk_version} PID: {pid}")
            self.kl.start()
            # Stop the listener
            self.kl.stop()
        except KeyboardInterrupt:
            self.kl.stop()
            print("PHK stopped.")

    @staticmethod
    def set_userscripts_folder(folder_name: str) -> None:
        """
        Sets the folder where the user scripts are located.
        When this is not specifically set the 'user_default' folder will be used.
        The 'user_default' folder is located under the 'user_root' folder.
        Both constants are defined in app/config/__init__.py
        :param folder_name: Name of the user scripts folder.
        """
        app.user.context_folder = folder_name

    def set_user_preferences(self) -> None:
        """
        Read preferences from the config.ini file and make them available in the application
        TODO: consolidate all setting in this function
        """
        ini = Ini()
        app.user.phrase_type_interval = ini.read_float_value("phrase_type_interval",
                                        fallback=fallback_phrases.type_interval)
        app.user.hotkey_type_interval = ini.read_float_value("hotkey_type_interval",
                                                             fallback=fallback_hotkeys.type_interval)


    def _write_key_reference_file(self) -> None:
        """
        (Over)Write a text file containing a reference of all
        hotkeys and phrases that are available at this moment.
        This can be used as a (printed) reference.
        """
        phrases = Phrase(self.kl).get_key_reference()
        hotkeys = Hotkey(self.kl).get_key_reference()
        key_reference_file = os.path.join(app.system.phk_run_path,
                                          app.user.root_folder,
                                          app.user.key_reference)
        with open(key_reference_file, "w") as file:
            file.write(phrases + "\n\n" + hotkeys)

    def pickle_files(self) -> None:
        """
        Save the hotkeys and phrases to pickled files for use of the DTM
        """
        hotkey_pickle = app.dtm.hotkeys
        phrase_pickle = app.dtm.phrases
        try:
            with open(hotkey_pickle, "wb") as f:
                pickle.dump(self.kl.hotkeys, f, pickle.HIGHEST_PROTOCOL)
                log.debug("Save pickle: %s", hotkey_pickle)
        except FileNotFoundError:
            log.error(f"Could not save pickle: {hotkey_pickle}", exc_info=True)
        try:
            with open(phrase_pickle, "wb") as f:
                pickle.dump(self.kl.phrases, f, pickle.HIGHEST_PROTOCOL)
                log.debug("Save pickle: %s", phrase_pickle)
        except FileNotFoundError:
            log.error(f"Could not save pickle: {phrase_pickle}", exc_info=True)


if __name__ == "__main__":
    args = get_args()
    print("Starting PHK...")
    # make sure that pkill will have enough time to kill a possible previous instance
    time.sleep(3)
    phk = Phk()
    phk.set_userscripts_folder(args.user_scripts_folder)
    phk.set_user_preferences()
    app.system.phk_pid = os.getpid()  # TODO move this to app/__init__?
    log.debug(f"OS:{app.system.os_name,}, Python:{app.system.python_version}")
    log.debug(f"PHK version:{app.version.number,}, "
              f"PHK release date:{app.version.release_date}")
    phk.start(app.version.number, app.system.phk_pid)

