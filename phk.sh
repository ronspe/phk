#!/bin/bash
# xhost +SI:localuser:$USER
if [ $# -eq 0 ]
    then
    echo "No arguments supplied!"
    echo "Provide a script directory to load or kill the PHK process."
    echo "Usage:"
    echo "Start PHK: phk script_directory e.g: phk example"
    echo "Stop  PHK: phk --kill  OR:  phk -k"
elif [[ $1 == --kill ]] || [[ $1 == -k ]];
    then
    echo "Killing the PHK process!"
    pkill -9 -f phk.py
else
    # This will kill the previous session of PHK to avoid conflicts
    pkill -9 -f phk.py
    # Now we can restart PHK safely
    .venv/bin/python3 phk.py $1 &
    cat scripts/key_reference.txt
fi
