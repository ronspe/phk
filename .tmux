#!/bin/sh
# This file needs to be executable

set -e

TMUXSESSION='PHK'

if tmux has-session -t=$TMUXSESSION 2> /dev/null; then
    tmux attach -t $TMUXSESSION 
else
    WINDOWNAME='vim'
        tmux new-session -d -s $TMUXSESSION -n $WINDOWNAME -x $(tput cols) -y $(tput lines)
        tmux split-window -t $TMUXSESSION:$WINDOWNAME -h

            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.left 'source .venv/bin/activate' Enter
            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.left "vim -c Files" Enter

            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.right 'source .venv/bin/activate' Enter
            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.right 'ctags -R .' Enter
            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.right 'cscope_py' Enter
            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.right 'clear' Enter
            tmux send-keys -t $TMUXSESSION:$WINDOWNAME.right "git status" Enter

    tmux attach -t $TMUXSESSION:$WINDOWNAME.right
fi
