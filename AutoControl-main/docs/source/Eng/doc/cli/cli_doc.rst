CLI
----

We can use the CLI mode to execute the keyword.
json file or execute the folder containing the Keyword.json files.

The following example is to execute the specified path of the keyword JSON file.

.. code-block::

    python je_auto_control --execute_file "C:\Users\JeffreyChen\Desktop\Code_Space\AutoControl\test\unit_test\argparse\test1.json"



The following example is to run all keyword JSON files in a specified folder:

.. code-block::

    python je_auto_control --execute_dir "C:\Users\JeffreyChen\Desktop\Code_Space\AutoControl\test\unit_test\argparse"