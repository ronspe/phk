---
title: 'macro.shell:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]


The shell functions are automatically available when you import the macro library:

Example: `import app.macro as m`

# macro.shell
Shell related functions

## executable_found
Checks if an executable program is found.

Definition: `executable_found(prog: str) → bool`

Parameters:
- prog – The command you want to find.

Returns: True or False

Example: 
```python
if m.shell.executable_found("ls"):
    m.shell.run_command("ls -la *.log")
```

## osx_executable_found
Checks if an executable program is found in OSX.

Definition: `osx_executable_found(prog: str) → bool`

Parameters:
- prog – The command you want to find.

Returns: True or False

Example: 
```python
if m.shell.osx_executable_found("brew"):
    m.shell.run_command("brew -h")
```

## return_output
Execute a shell command and return the output.

Definition: `return_output(cmd: str) → str`

Parameters:
- cmd – The command to execute.

Returns: The output of the command as a string.

Example: `line_count = m.shell.return_output("wc -l output.txt")`

## run_command
Execute a shell command.

Definition: `run_command(cmd: str) → None`

Parameters:
- cmd – The command to execute.

Example: `m.shell.run_command("cd ~; mkdir new_folder")`


## start_a_browser
Start or open a web browser with an url. 

This useful when you do not know which browser is installed.

Definition: `start_a_browser(url: str, browsers: list = None) → bool`

Parameters:
- url – The url that needs to be opened.
- browsers – A list of browsers that can be opened. 

For each browser in the list, it is first checked if the browser is found. 
You can change the list and the order according to your preferences. 

Default = [“firefox”, “chromium-browser”, “google-chrome”, “opera”, “microsoft-edge”, “iexplore”]

Returns: 
- True if a browser is started, 
- False if no browser can be found.

Example: `m.shell.start_a_browser("http://.www.google.com", ["opera", "firefox"])`

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
