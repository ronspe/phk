---
title: 'macro.system:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]


# macro.system:
Contains convenience functions for checking on which machine the script runs.

The system functions are automatically available when you import the macro library:

Example: `import app.macro as m`



## os_platform
Returns a unified name of the operation system.

Definition: `static os_platform() → str`

Returns: “linux”, “mac” , “windows”, “cygwin”, “solaris” or “unknown”

Example: 
```python
if m.system.os_platform() == 'linux':
    # do something
elif m.system.os_platform() == 'mac':
    # do someting else
```

## os_version
Returns the detailed version of the OS

Definition: `os_version() → str`

Example: `my_os = m.system.os_version()`


## python_version
Returns the version of Python that your are currently runing

Definition: `python_version() → str`

Example: `python_version = m.system.python_version()`

## user_homedir
Returns the path to the home directory of the current user.

Definition: `user_homedir() → str`

Example: `my_home = m.system.user_homedir()`


## user_name
Get the username from the environment. If that fails try the password database. 

WARNING: Do not use this for sensitive scripts because the environment variables can be manipulated!

Definition: `user_name() → str`

Example: `text = f'Good morning {m.system.user_name()'`


[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
