---
title: 'macro.sound.play:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.sound.play:
Play different sounds. 

Useful as a warning when a time consuming operation ends.

The sound functions are automatically available when you import the macro library:

Example: `import app.macro as m`

## Available sounds:
- beep1
- beep2
- bell1
- bell2
- ping
- ting

## Definition: 
`sound_name(times: int = 1, interval: float = 0.5) -> None`

## Example 
```python
m.sound.play.beep1()
m.sound.play.bell2(times=3, interval=2)
m.sound.play.ting(times=3)
```

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
