---
title: 'macro.screen:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.screen:
Get information about the current screen.

The information about your screen is automatically available when you import the macro library:

Example: `import app.macro as m`

## Available values
- screen_width = The width of the screen
- screen_height = The height of the screen
- center_x = The x position of the center of the screen
- center_y = The y position of the center of the screen
- center = The (x, y) position of the center of the screen
- mouse_position = The current position of the mouse

## Examples
```python
w = m.screen.screen_width
h = m.screen.screen_height
mouse = m.screen.mouse_position
m.log.info(f"My screen is {w}x{h} pixels and my mouse is here: {mouse}")

```


[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
