---
title: 'macro.network:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]


# macro.network:
Network related functions

The network functions are automatically available when you import the macro library:

Example: `import app.macro as m`


## has_internet
Checks if there is an internet connection available by trying to connect to a website.

Definition: `has_internet(host: str = 'www.google.com', port: int = 80) → bool`

Parameters:
- host – Name of the host. Default google.com
- port – The port to check. Default =80

Returns: True or False

Example:
```Python
if m.network.has_internet():
    if m.shell.start_a_browser("https://www.wikipedia.com"):
        m.mouse.moveto.center()
        # do more stuff...
    else:
        m.log.error('The browser could not be opened.')
else:
    m.log.error('No internet!')

```

## hostname
Returns the hostname of the machine.

Definition: `hostname() → str`

Example: `my_pc = m.network.hostname()`

## mac_address
Returns the MAC address of the computer. 

Can be used to identify a computer in the network

Definition: `mac_address() → str`

Example: `mac = m.network.mac_address()`


[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)

