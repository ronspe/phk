---
title: 'macro.keys:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.keys:
Type characters or press multiple keys at the same time

The keyboard functions are automatically available when you import the macro library:

Example: `import app.macro as m`

## press
Press and release a combination of keys that should be pressed at the same wait_for. 

E.g.: "\<ctrl>\<home>" or "\<ctrl>c" 

By default we pause half a second to give the receiving application wait_for to resond to our keys.

Definition: `press(key_combination: str, pause: float = 0.5, times: int = 1) → None`

Parameters:
- key_combination – The keys that should be pressed at the same wait_for.
- pause – Pause in seconds after keypress. Default = 0.5
- times – Amount of times to press the key. Default = 1

Example: `m.keys.press('<ctrl>a', pause = 0.5, times = 1)`

## type
Type a text to the screen including special keys one by one. 

E.g.: “Kind regards,\<enter>John Smith” 

If you would like to press multiple keys at the same wait_for, use the `press()` function.

Definition: `type(text: str, raw: bool = False, interval: float = 0.05) → None`

Parameters:
- text – The text to type.
- raw – If True the text is typed without converting the codes for special keys. e.g.: raw=False : “a<tab>b” –> “a b” raw=True : “a<tab>b” –> “a<tab>b” Default is False.
- interval – This is the time in seconds between each character being typed Default = 0.05 seconds (typing speed) Set it to 0 for the fastest typing speed

Example:
```python
name = 'John'
m.keys.type(f'Hello, {name}<enter>', interval = 0)
```

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)

