---
title: 'macro.wait:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.wait:
Pause the macro until a specific event occurs. 

This is useful for parts of the macro that needs manual input.

These functions are automatically available when you import the macro library:

Example: `import app.macro as m`

## for_colour_change
Pause the macro until the pixel at coordinate (x, y) changes colour. 

Any change in colour will cause the macro to continue.

Definition: `for_colour_change(x: int, y: int, interval: int = 1) → tuple`

Parameters:
- x – Coordinate of x
- y – Coordinate of y
- interval – Interval in seconds to check. Default = 1 second

Returns: The current colours in rgb and hex format in a tuple. 

e.g.: ((253, 252, 237), ‘fdfced’)

Example: `m.wait.for_colour_change(100, 62, 1)`


## for_colour_hex
Pause the macro until the pixel at coordinate (x, y) changes to a specific colour.

Definition: `for_colour_hex(x: int, y: int, colour: str, interval: int = 1) → bool`

Parameters:
- x – Coordinate of x
- y – Coordinate of y
- colour – The hex colour code. e.g.: ‘#ffffff’ or ‘ffffff’
- interval – Interval in seconds to check. Default = 1 second

Returns: True

Example: 
```python
if m.wait.for_colour_hex(100, 150, '#c3c3c3'):
    # do something
```

## for_colour_rgb
Pause the macro until the pixel at coordinate (x, y) changes to a specific colour.

Definition: `for_colour_rgb(x: int, y: int, colour: tuple, interval: int = 1) → bool`

Parameters:
- x – Coordinate of x
- y – Coordinate of y
- colour – The RGB colour code in a tuple. e.g.: (255, 255, 255)
- interval – Interval in seconds to check. Default = 1 second

Returns: True

Example: 
```python
if m.wait.for_colour_rgb(x=100, y=150, colour=(100, 45, 32), interval=3):
    # do something
```
## for_key_press
Pause the macro until a key is pressed. 
The key can be a single key e.g: “\<enter>” or “\<tab>” or “a” or a combination e.g.: “\<ctrl>\<f7>” or “\<cmd>a”

Definition: `for_key_press(key_representation: str) → None`

Parameters:
- key_representation – A string representing one or more keys.

Example: `m.wait.for_key_press("<enter>")`

## for_mouse_click
Pause the macro until a mouse has clicked on a target on the screen. 
The target is a rectangle defined by the top_left and bottom_right position on the screen.

Definition: `for_mouse_click(top_left, bottom_right, button: str = 'left') → tuple`

Parameters:
- top_left – Top left position of the target (x, y)
- bottom_right – Bottom right position of the target (x, y)
- button – The button that must be clicked. Default is “left” possibilities: [left, right, middle]

Returns: The coordinates of the click (x, y)

Example:
```python
# coordinates for the target to click in
top_left = (0, 0)
bottom_right = (500, 500)
# wait for the mouse click until it hits the target
coord = m.wait.for_mouse_click(top_left, bottom_right)
```

## seconds
Pause the script for X seconds. The default is 1 second.

Definition: `seconds(seconds: float = 1) → None`

Parameters:
- seconds – The amount of seconds to pause. 
  - This can be an integer: pause(3) 
  - This can be a decimal: pause(1.5) 
  - Or without seconds: pause() This defaults to 1 second.

Example: `m.wait.seconds(10)`

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
