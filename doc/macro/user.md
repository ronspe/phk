---
title: 'macro.user:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.user:
Information about the installation path of the PHK macro scripts

The user information are automatically available when you import the macro library:

Example: `import app.macro as m`

## Available values:
- root_folder = The path where PHK is installed
- default_folder = The path to the default script folder

## Example
```python
import os
import app.macro as m

phk_log = os.path.join(m.user.root_folder, 'phk.log')
reference = os.path.join(m.user.root_folder, 'scripts', 'key_reference.txt')
```



[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)

