---
title: 'macro.clipboard:'
---

[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.clipboard:¶
Functions to manipulate the system clipboard. 

The clipboard functions are automatically available when you import the macro library:

Example: `import app.macro as m`

## copy
Copy a text to the clipboard.

Definition: `copy(text: str) → None`

Parameters:
- text – The text to copy.

Example: `m.clipboard.copy('Hello world')`

## empty
Empty the clipboard. 

Definition: `empty() → None`

Note: If you use a clipboard manager, the history of your clipboard is still visible in your clipboard manager.

Example: `m.clipboard.empty()`

## get
Get the text from the clipboard as a return value. The text will not be typed or pasted.

Definition: `get() → str`

Example: `clipboard_text = m.clipboard.get()`

## paste
Print the text from the clipboard to stdout.

Definition: `paste() → None`

Example: `m.clipboard.paste()`

## type
Type the text from the clipboard.

Definition: `type() → None`

Example: `m.clipboard.type()`


[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
