---
title: 'macro.network:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]


# macro.mouse:
Handeling the mouse. 

These functions are mostly wrappers around PyAutoGui functions with logging implemented for debugging.

The mouse functions are automatically available when you import the macro library:

Example: `import app.macro as m`


## click
You can click left, right and middle click on a coordinate.

Definition: `left(self, coord: tuple=None, clicks: int=1, interval: float=0.25, wait: float=0.5) -> tuple` 

Parameters:
- coord - The coordinate where the mouse should click. (x, y)
  - If no coordinate is given the current mouse position is used.
- clicks - The amount of clicks, default is one click
- interval - If multiple clicks: The interval in seconds between clicks.
  -  Default is 0,25 seconds.
- wait - The amount of time in seconds to wait after the click to give the application some  time to react. 
  - Default = 0.5 seconds

Return: The coordinates of the click (x, y)

Example:
```python
# click three times on a coordinate
m.mouse.click.left(coord=(100, 345), clicks=3, interval=0.5)
# click once on the current mouse position
m.mouse.click.left()
# right-click on a coordinate
m.mouse.click.right((200, 150))
# middle click two times
m.mouse.click.middle(clicks=2)
```

## drag
Move the mouse to a new coordinate while keeping the mouse button down (drag).

Definition: `coord(coord: tuple, seconds: int=1, button: str='left') -> None`

Parameters:
- coord - The coordinate where the mouse should go to. (x, y)
- seconds - The amount of wait_for it should take the mouse to move from the current position to desired offset.
  - Default is 0 seconds.
- button - The button that should be hold. 
  - Default is 'left'.
  
Example: `m.mouse.drag.coord(100, 350), 4, 'left`)

## move
Relative movements of the mouse. No clicking.

Definition of Up, Down, Left, Right functions:
```
down(down: int, seconds: int=0) -> None
up(up: int, seconds: int=0) -> None
left(left: int, seconds: int=0) -> None
right(right: int, seconds: int=0) -> None
```

Parameters:
- [direction] - The amount of pixels to move in this direction
- seconds - The amount of seconds it should take the mouse to move from the current position to desired offset.
  - Default is 0 seconds.

Definition of relative function: `relative(offset: tuple, seconds: int=0) -> None`

Parameters:
- offset - The offset to move. (x, y)
- seconds - The amount of seconds it should take the mouse to move from the current position to desired offset.
  - Default is 0 seconds.
  
Example:
```python
m.mouse.move.down(200)
m.mouse.move.left(200, seconds=2)
m.mouse.move.up(200)
m.mouse.move.relative((10, -5))
```

## moveto
Move the mouse to a coordinate or fixed positions, no clicking.

Move the mouse to a coordinate.

Definition: `coord(coord: tuple, seconds: int=0) -> None`
       
Parameters: 
- coord - The coordinate where the mouse should click. (x, y)
- seconds- The amount of wait_for it should take the mouse to move from the current position to the desired coordinate.
  - Default is 0 seconds.
  
Move the mouse to center, top, bottom, farleft, farright positions.
Definitions:
```
center(seconds: int=0) -> None
top(seconds: int=0) -> None
bottom(seconds: int=0) -> None
farleft(seconds: int=0) -> None
farright(seconds: int=0) -> None
```
Parameters: 
- seconds- The amount of seconds it should take the mouse to move from the current position to the desired coordinate.
  - Default is 0 seconds.

Examples:
```python
m.mouse.moveto.center()
m.mouse.moveto.top()
m.mouse.moveto.bottom()
m.mouse.moveto.farleft()
m.mouse.moveto.farright()
```

## scroll
Scroll with the mousewheel. (Linux only) 

You can scroll Up, Down, Left, Right.

Definitions:
```
up(up: int) -> None
down(down: int) -> None
left(left: int) -> None
right(right: int) -> None
```

Parameters:
- [direction] - The amount of pixels you want to scroll in this direction

Examples:
```python
m.mouse.scroll.left(50)
m.mouse.scroll.down(1000)
```

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)

