---
title: 'macro.log:'
---
[Back to Index](../index.md)

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[[_TOC_]]

# macro.log:
Add specific logging messages to the file `scripts/macro.log` and/or the stdout.

The `scripts/macro.log` file is overwritten every time a macro is executed.

Use this for debugging your macro. These functions are here for your convenience, no need to set up logging.

Most other macro functions will also write to the `scripts/macro.log` file, making it easy to see what is happening in your
macro without writing `print()` and `logging.log()` statements all over your code.

The logging functions are automatically available when you import the macro library:

Example: `import app.macro as m`

## critical
Log critical messages. Critical messages appear on the output and in the macro.log.

Definition: `critical(message: str) → None`

Parameters:
- message – The message in the log.

Example: `m.log.critical('Describe what went wrong here...')`

## error
Log error messages. Error messages appear on the output and in the macro.log.

Definition: `error(message: str) → None`

Parameters:
- message – The message in the log.

Example: `m.log.error('Describe what went wrong...')`

## warning
Log warning messages. Warning messages appear on the output and in the macro.log.

Definition: `warning(message: str) → None`

Parameters:
- message – The message in the log.

Example: `m.log.warning('Describe the warning...')`

## info
Log info messages. Info messages appear on the output and in the macro.log.

Definition: `info(message: str) → None`

Parameters:
- message – The message in the log.

Example: `m.log.info('Describe the step in the process...')`

## debug
Log debug messages. Debug messages appear only in the macro.log, they do not show on the output.

Definition: `debug(message: str) → None`

Parameters:
- message – The message in the log.

Example: `m.log.debug('Describe the step in the process...')`

[Back to Macro documentation](../macro_documentation.md#macro-functions)

[Back to Index](../index.md)
