[Back to Index](index.md)

[[_TOC_]]

Phrase functions: expanding text on steroids
============================================

A "Phrase function" is a function that is used to expand text just like the ["regular phrases"](phrases.md) but it is much more flexible then "If I type this exact string: then expand to ..."

The functionality of a "Phrase function" is totally up to you.

You can use regular expressions, fuzzy matching of strings, exluding certain strings etc.

For PHK a "Phrase function" is a black box. PHK will execute your function on every keystroke and feed it the typed text.
It expects back an empty list or a list with text to replace and text to type.

Understand how a Phrase function works
--------------------------------------

To understand how a Phrase function works, you have to know how PHK implements the normal Phrases.

A phrase has a "trigger", this is the text that will trigger the text expansion and a "replacement text"

When you type a character on the keyboard, PHK checks if the last characters of the typed text are present in one of the Phrase triggers.

If it finds that the trigger is in one of the Phrases it will count the amount of characters in the trigger and hit the backspace key X times to remove the trigger text.

Then it will type the replacement text.

It will remove the typed characters from memory and start over again waiting for the next Phrase to expand.

The "Phrase function" therefore need to have a parameter that uses the typed text and it needs to output the text that needs to be removed so PHK can backspace the trigger and it needs to provide the text that needs to be typed.

Rules of a Phrase function
--------------------------

- PHK will recognise a "Phrase function" by the decorator: `@phrasefunction()`.
  -   The decorator can have a priority parameter. The default value is 100.
      -   This parameter is used in case more then one Phrase function will return a result.
      -   The function with the highest prio will win and only this function will be executed.
- The script should be located in a `/scripts` sub-folder
- The Phrase function has only one parameter named: "text"
-  The output must be list with two string values:
    -  [text_to_remove, text_to_type\] e.g.: return ["this", "that"]
    -  If there is no result the Phrase function must return an empty list.
- If there is a normal Phrase defined that will also match the trigger, only the normal Phrase will be expanded. The Phrase function will not be executed to avoid a possible chain reaction on the already expanded text.

Order of execution
------------------

-   If there is a Phrase that match the trigger: ONLY the Phrase will expand.
-   If there is no Phrase that match the trigger: Check if there is a Phrase function that does.
    -   If there is one Phrase function that matches: execute it.
    -   If there are more then one Phrase function that matches: execute only the one with the highest prio.

Script to add a "Phrase function"
---------------------------------

```python
from app.script import Phrasefunction

@phrasefunction(100)
def example(text: str) -> list:
    # Do something interesting here...
    if something_interesting_happened:
        text_to_remove = text
        text_to_type = "Replacement"
        result = [text_to_remove, text_to_type]
    else:
        result = []
    return result
```

Explanation:

-   Line 1: **Phrasefunction** is imported, this is used on line 3 as the decorator
-   Line 3: The decorator makes it a "Phrase function" The decorator can have a priority parameter.
-   Line 4: The function has only one parameter of the type "str".
-   Line 9: The return value is a list with the text to remove and the text to type.
-   Line 11: If there is nothing to expand, we return an empty list
-   Line 12: Returning the result

Examples of "Phrase functions"
------------------------------

Check the the file [scripts/examples/phrasefunctions.py](https://gitlab.com/ronspe/phk/-/blob/master/scripts/example/phrasefunctions.py) for some more useful examples of a "Phrase function".

The examples now contains functions for:

-   Removing consecutive duplicate words while you type using a regular expression.
    -   e.g.: "This is is " --&gt; "This is "
-   Correcting consecutive duplicate uppercase characters at the beginning of a word while you type, using a regular expression.
    -   e.g.: " TH" --&gt; " Th"
-   Fuzzy match a list of strings and do a replacement.
    -   This example uses a special trigger using two spaces to only expand on my command because fuzzy matching can match any text very quickly.
    -   The example has a list of cities like: cities = ["Bridgeport", "Greensboro", "Greenville"]
    -   e.g.: when I type:
        -   "&lt;space&gt;port" should expand to "Bridgeport"
        -   "&lt;space&gt;green" should not expand because it has two possible matches.
        -   "&lt;space&gt;greeno" should expand to "Greensboro"

If you have made a "Phrase function" that you want to share and could be of use to other users, let met know!

[Back to Index](index.md)
