[Back to Index](index.md)

[[_TOC_]]

Remembering triggers for hotkeys and phrases
============================================

In PHK you can organise your hotkeys and phrases in multiple folders and files which makes it easy to forget which triggers are available for you.

Therefor PHK has two solutions:

 -   a text file which contains a list of all currently loaded hotkeys and phrases.
 -   a pop-up menu which lists all hotkeys and phrases.

key_references.txt
-------------------

The text file is located in the folder `\scripts` and the file is named: `key_reference.txt`

It looks like this:

```text
7 Available phrases:
Trigger : Description
    --- : -----------
     ,p : html <p>
    ,br : html <br>
    ,h1 : html <h1>
    ,h2 : html <h2>
    ,nb : html &nbsp;
    ;kr : Kind regards
    ;mm : mymail

8 Available hotkeys:
           Trigger : Description
               --- : -----------
         <cmd><f1> : Toggle touchpad
         <cmd><f2> : Toggle keyboard
      <ctrl><alt>0 : Play last recorded macro
      <ctrl><alt>= : Record macro
     <shift><cmd>c : Chromium
     <shift><cmd>o : Office
     <shift><cmd>v : Vim
     <shift><cmd>w : Firefox
```

When you define a hotkey or a phrase, the last value was a short description. This short description is used in this overview.

The file `key_reference.txt` is overwritten everytime PHK starts.

Pop-up menu (DTM)
-----------------

If you have installed the Tkinter package, you will be able to use the pop-up menu.

This popup menu is called the "Double Tap Menu" in the documentation and the code.

The DTM can be accessed by quickly tapping the &lt;capslock&gt; key twice.

This will show a menu like this:

![image](_static/PHK_Double_tap_menu_001.png)

You can fuzzy search the trigger that you are looking for.
If you hit the &lt;Enter&gt; key the first trigger in the filtered list will be executed. Close the DTM window with &lt;esc&gt;.

You can configure the look and feel of the DTM with settings in the file: `scripts/config.ini`
- You can set the key to open the DTM menu. Default is &lt;capslock&gt;
- You can set the x-y position on the screen where the window will open. Default = (100, 100)
- You can set the width and height of the list of triggers. Default = (30, 10)

  This will also influence the width and height of the window.
- You can set the color theme of the window to your taste. Default = "DarkGrey6"

  For an overview of available themes look here: <https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion>

**Tip:** PKH makes use of the wonderful pySimpleGUI package for the few GUI interactions that we need.

PySimpleGUI is capable of making complex GUI's but is also perfect for making simple user interactions in your scripts!
Since this is already installed and available it is easy to implement in your macro scripts:

```python
import PySimpleGUI as sg
sg.Popup('Hello From PySimpleGUI!', 'This is the shortest GUI program ever!')
```

See: <https://pysimplegui.readthedocs.io/en/latest/> for the documentation and examples.

[Back to Index](index.md)
