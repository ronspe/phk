[Back to Index](index.md)

[[_TOC_]]

Starting and stopping PHK
=========================

In the root of the PHK folder is a script: phk.sh

You can start PHK with: `./phk.sh default`

The parameter "default" means you will load the triggers that are
defined in folder: `scripts/default` into memory.

For example you can try PHK out and load the example scripts like
this: `./phk.sh example`

This will allow you to load Phrases and Hotkeys in a specific context.
For example, you can share certain hotkeys on all your laptops but
have some hotkeys specific for Linux or Mac.
Or you can start PHK in context to a specific project you are working
on at this moment.

If you start PHK without a parameter PHK will not start.

In the file scripts/config.ini you can edit a list of folder names that, when they are used as a context, the default folder will not be loaded.

This allows for testing/debugging a limited amount of phrases and hotkeys.
e.g.: 

`no_default_folders = ["TEST" ,"test", "example"]`

Restarting PHK
--------------

There can be only one instance of PHK loaded into memory.

If you start PHK for the second time, the previous instance of PHK will be killed to avoid conflicts.

This is an easy way to switch context (load another script directory into memory) or to reload changed hotkeys or phrases.

Stopping PHK
------------

You can kill PHK by providing the parameter `--kill` or `-k`

e.g.: `./phk.sh --kill`

Using the same triggers (hotkeys and phrases) for more then one purpose:
------------------------------------------------------------------------

There is always a shortage of possible key combinations to trigger an action.

PHK offers the possibility to use the same key combination for different actions.

For example:

 -  for "Project1" you always use the key combination e.g.: \[ctrl-1\] to open your editor.
 -  for "Project2" you need to open another editor but you would also like to use \[ctlr-1\] because it is in you muscle memory.

You can create different folders for each project and add project specific scripts to that folder.
On the day that you are working on "Project1" you can start PHK with the correct folder as parameter.

The "default" folder will always be loaded but before the -in this case- "Project1" folder.

When a trigger is loaded more then once the latest will be used. This means that you can use the same trigger in a project folder that will overwrite the one in de "default" folder.

Overwriting of triggers happens even within a folder.

A folder is read alphabetically and a trigger present in script `a.py` but also in `b.py` will be overwritten by the trigger in `b.py`.

Prevent loading of triggers (hotkeys and phrases)
-------------------------------------------------

If you do not want to load triggers in a script that is present in the folder: prefix the name with an underscore.

e.g.:

 -   `myscript.py` will be read
 -   `_myscript.py` will be ignored

Script folders
--------------

There are some script folders already present when you install PHK:

 -   `default` This is the folder that will always be loaded.

     These scripts are stored in this folder:

      -   `_recorded_template.py` This is used for the macro recorder
      -   `macro_recorder.py` This starts the macro recorder
      -   `recorded.py` This is the recorded macro.

 -   `example` This folder contains examples of scripts for your
     reference and inspiration.
 -   `linux` This folder contains `start_apps.py` with some triggers
     for starting Linux applications
 -   `mac` This folder contains `start_apps.py` with some triggers for
     starting MacOS applications

[Back to Index](index.md)
