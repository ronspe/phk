[Back to Index](index.md)

[[_TOC_]]

Installing PythonHotKey (PHK)
=============================

There is no installer (yet). Installation need to be done manually.

The steps involved:

  - installing Python, Pip and a virtual environment.
  - installing needed libraries and Python packages

Install on Linux (Ubuntu)
-------------------------

 -   Install Python3. See: <https://docs.python.org/3/using/unix.html>
 -   Install Pip. See: <https://pip.pypa.io/en/stable/installing/>
 -   

     Install Python Virtual environment:
```bash
sudo apt install python3-venv
```

 -   Install Tkinter: (needed for graphical interface)
```bash
sudo apt-get install python3-tk python3-dev

```

 -   Install needed ffmpeg libraries (optionally)
```bash
sudo apt-get install ffmpeg libavcodec-extra
```

 -   Download the PythonHotKey files on your PC.
 -   Change to the new PythonHotKey directory.
 -   In this directory make a new virtual environment:
```bash
python3 -m venv .venv
```

 -   Activate the virtual environment:
```bash
source .venv/bin/activate
```
 -   Install the needed packages into this virtual environment:
```bash
 pip3 install -r requirements.txt
```



[Back to Index](index.md)
