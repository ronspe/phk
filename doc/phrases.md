[Back to Index](index.md)

[[_TOC_]]

Phrases: expanding typed abbreviations
======================================

"Phrases" is the term PHK uses for text expansion and autocorrecting typos.

 -   Auto correct: it will replace mistyped words with correctly typed ones, wherever you type them.

     For example: "hte" --&gt; "the"
 -   Text expansion: Make repetitive text entry faster and easier.

     For example: "m@" --&gt; "my-very-long-email-addres@ mail.com"

     **Tip**: for text expanding phrases; start them with a character you don't often use, such as ";" or "\`".

     For example: ";co" --&gt; "Copyright © 2020 by Wily E. Coyote

        All rights reserved. This book or any portion thereof may not be reproduced or used in any manner whatsoever without the express written permission of the publisher"

You can organize your phrases into different files. PHK will gather all phrases in a folder. This folder is the parameter that you will use when you start PHK. See ["How does it work?"](how_does_it_work.md)

In these Python files can be a list named: "phrases[]" which will store the defined phrases. PHK will gather all phrases in all files in this folder.

Script to add phrases:
----------------------

```python
from app.script import Expand

phrases = [
    [Expand, "<space>btw<space>", " by the way ", "btw"],
    [Expand, "<space>kr<enter>", "Kind regards,<enter>John", "kind regards"],
    [Expand, "mymail", "mymail@yahoo.com", "mymailYahoo"],
    [Expand, "mymail", "othermail@yahoo.com", "myothermailYahoo"],
]
```

Each phrase definition consists of a list of four values:

 1.  The script-action: **Expand**
 2.  The typed keys that will trigger the expansion.
 3.  The text that will replace the typed keys.
 4.  A short description of the phrase.

Notice:

 -   The first phrase triggers "btw" only when it is surrounded by spaces.
 -   The second phrase also add a linefeed &lt;enter&gt; in the expansion.
 -   The third and the fourth phrase have the same trigger, the third phrase will be overwritten by the fourth phrase.

Typing speed of phrases
-----------------------

In the file scripts/config.ini you can set the interval between two typed keys in seconds.

The default value is 0.05 seconds, that is approx. typing speed.

If you want the fasted typing speed, set it to zero (0)

e.g.:
```ini
phrase_type_interval = 0.05
hotkey_type_interval = 0.05
```

You can set the typing speed of hotkeys (with an action of "Type") to a different value.

For more advanced use of Phrases, check out ["Phrase functions"](phrasefunctions.md)

[Back to Index](index.md)
