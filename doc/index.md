---
title: 'PythonHotKey''s (PHK) documentation'
---
[[_TOC_]]

What is this?
=============

PythonHotKey is a Python application that can be used to emulate  keystrokes, move and click the mouse, launch programs, and open documents.

These actions can be triggered by hotkeys like \[F7\] or \[ctrl + 4\] or by a typed phrase.

PythonHotKey (PHK) tries to be an alternative for AutoHotKey on Linux and MacOS.

PHK is build on top of the PyAutoGUI and Pynput packages.

PHK is currently only tested on Linux (Xubuntu) with Python 3.6 and only limited tested on a Macbook Air with Python 3.7.

The PHK [macro-scripts](macro_documentation.md) can be written in pure Python.

There is a [macro library](macro_documentation.md#macro-functions) that makes writing the macros very easy.

There is even a [macro recorder](macro_documentation.md#macro-recorder) that can do most of the work for you.

Current status
==============

PHK is still very much in development. I have used it daily for a year on my PC (XUbuntu 18.04) with Python 3.6
I have also did limited testing on MacOS with Python 3.7.

There is no installer and limited documentation which makes the [installation](installation.md) a bit difficult if you are not familiar with Python.

To make PHK mature it will need contributers to test and fix issues on different Linux distributions and on MacOS.

If you are interested to contribute please contact me: ronaldDOTspeelmanATgmailDOTcom

The current version is 0.0.4
View the [CHANGELOG](../CHANGELOG.md)

Documentation
=============
1. [Installation](installation.md)
1. [How does PHK work?](how_does_it_work.md)
   1. [Starting and stopping PHK](how_does_it_work.md#starting-and-stopping-phk)
      1. [Switch context, reload hotkeys](how_does_it_work.md#restarting-phk)
      1. [Stopping PHK](how_does_it_work.md#stopping-phk)
   1. [Reusing hotkeys by switching context](how_does_it_work.md#using-the-same-triggers-hotkeys-and-phrases-for-more-then-one-purpose)
   1. [Prevent a script with hotkeys or phrases from loading](how_does_it_work.md#prevent-loading-of-triggers-hotkeys-and-phrases)
1. [Hotkeys:](starting_applications.md)
    1. [Starting applications](starting_applications.md#starting-applications-documents-and-scripts-with-phk)
    1. [Script to start an application](starting_applications.md#script-to-start-an-application)
    1. [Starting a Python script](starting_applications.md#starting-a-python-script)
    1. [How do I remember what hotkeys I have in use?](remembering_triggers.md)
    1. [Macros](macro_documentation.md)
        1. [Example macro](macro_documentation.md#example-macro)
        1. [Macro log file](macro_documentation.md#macro-logfile)
        1. [Macro recorder](macro_documentation.md#macro-recorder)
1. [Phrases:](phrases.md)
    1. [Simple text expansion](phrases.md)
    1. [Script to add a phrase](phrases.md#script-to-add-phrases)
    1. [Typing speed of a phrase](phrases.md#typing-speed-of-phrases)
    1. [Phrasefunctions: Text expansion on steroids](phrasefunctions.md)

Code on GitLab
==============

You can find the code here: <https://gitlab.com/ronspe/phk>
