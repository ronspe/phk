[Back to Index](index.md)

[[_TOC_]]

Starting applications, documents and scripts with PHK
=====================================================

You will typically want to start an application, open a specific document or run a Python or Bash script with a hotkey.

PHK will gather all hotkeys in a folder. This folder is the parameter that will use when you start PHK.

In this folder can be multiple Python scripts. See: ["How does it work?"](how_does_it_work.md)

In these Python files can be a list named: "hotkeys[]" which will store the defined hotkeys.
PHK will gather all hotkey definitions in all files in this folder.

Script to start an application:
-------------------------------

```python
from app.script import Run

hotkeys = []

hotkeys.append([Run, "<shift><cmd>c", "chromium-browser&", "Chromium"])
hotkeys.append([Run, "<shift><cmd>f", "thunar&", "Thunar"])
hotkeys.append([Run, "<shift><cmd>g", "gimp&", "Gimp"])
```

Each hotkey definition consists of a list of four values:

 1.  The script-action: **Run**
 2.  The key combination that will trigger the action.
 3.  The command to run from the commandline.
 4.  A short description of the command.

Starting a python script:
-------------------------

```python
from app.script import RunPython

hotkeys = []

script_path = "path/to/my/perfect_python_script.py"
parameters = "--param1=blabla"
cmd = "f{script_path} {parameters}"
hotkeys.append([RunPython, "<ctrl><alt>2", cmd, 'Perfect Python script'])
```

Notice that the script action is: **RunPython**

[Back to Index](index.md)
