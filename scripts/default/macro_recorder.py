# -*- coding: utf-8 -*-
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import app.phk.recorder as recorder

from app.script import RunPython, this_file

THIS_FILE = this_file(__file__)

start_keys = "<ctrl><alt>0"
stop_keys = "<ctrl><alt>-"
play_keys = "<ctrl><alt>="

hotkeys = [[RunPython, start_keys, THIS_FILE, "Record macro"]]

def main() -> None:
    """Start the macro recorder."""
    try:
        recorder.start(stop_keys, play_keys)
    except KeyboardInterrupt:
        print("MacroRecorder stopped with Ctrl-c")


if __name__ == "__main__":
    main()
