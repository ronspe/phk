# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

# Use pySimpleGUI for the popup window
import PySimpleGUI as sg
sg.theme("LightBlue3")
# More themes: https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion

THIS_FILE = this_file(__file__)
hotkeys = []

hotkeys.append([RunPython, "<ctrl><alt>5", THIS_FILE, "Clipboard demo"])


"""
The script will demo:
- copy some text to the clipboard
- open Firefox browser with an online editor
- type the text from the clipboard in the editor
- copy the first line to the clipboard
- change the text from the clipboard
- copy the changed text
- type the new text in the editor
"""


def main():
    # open an online editor with any available browser:
    # if m.shell.start_a_browser("https://write-box.appspot.com"):
    if m.shell.start_a_browser("https://anotepad.com"):

        # give it some time to load
        m.wait.seconds(8)
        m.keys.press("<tab>")
        # three lines of text:
        lines = "line 1 is a very nice line<enter>line2 is OK<enter>line3 is a bit longer<enter>"
        # copy the text to the clipboard
        m.clipboard.copy(lines)
        m.clipboard.type()
        # convert the text to upper case
        m.clipboard.copy(m.clipboard.get().upper())
        sg.PopupOK(m.clipboard.get(), title="Content of clipboard", font=("Calibri", 14))
        msg = "The demo is over...."
        sg.PopupOK(msg, title="PHK macro demo", font=("Calibri", 14))
    else:
        print("A browser could not be started.")



if __name__ == "__main__":
    main()
