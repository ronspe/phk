# -*- coding: utf-8 -*-
"""
This is a helper script used in the bad_quote.py script.
The script will:
- Open an editor
- Type some text in the editor
- Get random quotes from the internet.(change the url to make it more useful)
- Write the quotes in the editor

HEADS-UP If running on Mac:
Please double-click on /application/Python 3.X/Install Certificates.command'
This allows Python to access https urls
"""

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

# Import the library with 'macro' function.:
import app.macro as m

# Other standard Python library imports:
# https://docs.python.org/3/library/
import argparse
import urllib.request
import json
import random

def main(number_of_quotes):
    # Open an online editor in any available browser:
    if m.shell.start_a_browser("https://write-box.appspot.com"):
        # Give it some wait_for to load
        m.wait.seconds(4)
        # Write the first line
        m.keys.type("Quotes from BreakingBad:<enter>")
        # Download json file with quotes from the internet
        m.wait.seconds(4)
        json_obj = get_json()
        # Write the quotes in the editor
        for i in range(0, number_of_quotes):
            m.keys.type(get_bad_quote(json_obj))
    else:
        print("A browser could not be started.")


def get_json():
    url = "https://breaking-bad-quotes.herokuapp.com/v1/quotes/50"
    try:
        req = urllib.request.urlopen(url)
        data = req.read()
        return json.loads(data.decode('utf-8'))
    except Exception:
        m.log.error('If running on Mac: Please double-click on /application/Python 3.X/Install Certificates.command')
        print('If running on Mac: Please double-click on /application/Python 3.X/Install Certificates.command')


def get_bad_quote(json_obj):
    random_num = random.randint(0, 49)
    quote = json_obj[random_num]["quote"]
    author = json_obj[random_num]["author"]
    random_quote = "\"{}\"<enter>--{}<enter>".format(quote, author)
    return random_quote


if __name__ == "__main__":
    # Catch the optional commandline parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("--quotes", default=3, type=int)
    args = parser.parse_args()
    # Call our function with the commandline parameters
    main(args.quotes)

