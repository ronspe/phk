# -*- coding: utf-8 -*-
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))
import datetime
import argparse
import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

THIS_FILE = this_file(__file__)
hotkeys = []

"""
The example will print a header with a centered text or a timestamp.
Example output:
########################################
########### This is a header ###########
########################################


You can try this in an editor......

"""

params = "text --header='This is a header' --width=30 --filler='#'"
cmd = "{} {}".format(THIS_FILE, params),
hotkeys.append([RunPython, "<ctrl><alt>0", cmd, "Text header"])

params = "timestamp --width=30 --filler='-'"
cmd = "{} {}".format(THIS_FILE, params),
hotkeys.append([RunPython, "<ctrl><alt>1", cmd, "Timestamp header"])


def print_header(header, width: int=50, filler: str="-") -> None:
    """Prints a header with a text in the center to the terminal."""
    filler_line = "".center(width, filler) # line with only the filler
    header_line = header.center(width, filler) # line with header centered
    # The result must be send to the terminal, so use a print
    m.keys.type("<enter>{1}<enter>{0}<enter>{1}<enter>".format(header_line, filler_line)
                , interval=0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("type", help="The type of header: (text|timestamp)")
    parser.add_argument("--header", help="The text in the header")
    parser.add_argument("--width", default=50, type=int, help="Width of the header")
    parser.add_argument("--filler", default="-", help="Filler character")
    args = parser.parse_args()

    if args.type == 'text':
        print_header(args.header, args.width, args.filler)
    if args.type == 'timestamp':
        now = datetime.datetime.now()
        timestamp = " {} ".format(now.strftime("%a %Y-%m %H:%M"))
        print_header(timestamp, args.width, args.filler)
