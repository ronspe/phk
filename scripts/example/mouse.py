# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import random
import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

THIS_FILE = this_file(__file__)
hotkeys = []

hotkeys.append([RunPython, "<ctrl><alt>3", THIS_FILE, "Mouse demo"])

"""
The script will demo:
- open Firefox browser with an url
- go to full screen
- click in the center
- right-click in the center
- randomly click on the page
- exit full screen
"""


def main():
    # open firefox with an url
    if m.shell.start_a_browser("https://mrdoob.com/#/111/branching"):
        m.wait.seconds()
        # open in full screen
        m.keys.press("<f11>")
        # give it some time to load the page
        m.wait.seconds(7)
        # move the mouse to some fixed positions
        m.mouse.moveto.center()
        m.mouse.moveto.bottom()
        m.mouse.moveto.top()
        m.mouse.moveto.center()
        m.mouse.moveto.farleft()
        m.mouse.moveto.farright()
        m.mouse.moveto.center()
        m.mouse.click.left(clicks=3, interval=0.5)
        # move the mouse relatively 100 right and 100 up
        m.mouse.move.relative((100, -100), seconds=2)
        m.mouse.click.left()
        # make a square around the center
        m.mouse.move.down(200)
        m.mouse.click.left()
        m.mouse.move.left(200)
        m.mouse.click.left()
        m.mouse.move.up(200)
        m.mouse.click.left()
        m.mouse.move.right(200)
        m.mouse.click.left()
        # move back to the center
        m.mouse.moveto.center(seconds=2)
        # right click in the same position
        m.mouse.click.right()
        m.wait.seconds()
        # close the menu
        m.keys.press("<esc>")
        m.wait.seconds()
        # randomly click around
        for i in range(1, 20):
            click_random()
            m.wait.seconds(0.3)
        # exit full screen
        m.keys.press("<f11>")
    else:
        print("A browser could not be started.")


def click_random():
    shift_x = random.randrange(300) - 150
    shift_y = random.randrange(300) - 150
    new_x = m.screen.center_x + shift_x
    new_y = m.screen.center_y + shift_y
    new_position = (new_x, new_y)
    m.mouse.click.left(new_position)


if __name__ == "__main__":
    main()
