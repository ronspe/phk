# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import os
from app.script import RunPython, this_folder

THIS_FOLDER = this_folder(__file__)
hotkeys = []

"""
This example will:
- Open an editor
- Type some text in the editor
- Get random quotes from the internet.(change the url to make it more useful)
- Write the quotes in the editor
"""

script_path = os.path.join(THIS_FOLDER, "bad_quote_macro.py")
parameters = "--quotes=3"
cmd = "{} {}".format(script_path, parameters)
hotkeys.append([RunPython, "<ctrl><alt>2", cmd, 'Bad quotes'])
