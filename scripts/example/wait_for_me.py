# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

# Use pySimpleGUI for the popup window
import PySimpleGUI as sg
sg.theme("LightBlue3")
# More themes: https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion

this_file = this_file(__file__)
hotkeys = []

hotkeys.append([RunPython, "<ctrl><alt>6", this_file, "wait for key and mouse demo"])

"""
the script will demo how you can pause your script and resume it after
you typed a key or key-combination and pause the script until you
clicked within a certain area on screen.
this is helpful if you cannot 100% automate a task, but need some manual task
in between the parts of your script.
the script will:
- open an available browser with an online editor
- type some text in the editor
- pause the script until you type an <enter>
- resume the script and type some text in the editor
- pause the script until you click a target on the screen 
- resume the script and again show the clicked coordinates in a message box
"""


def main():
    # open an online editor:
    if m.shell.start_a_browser("https://anotepad.com"):
        m.wait.seconds(4)
        # make it full screen
        m.keys.press("<f11>")
        # give it some ime to load
        m.wait.seconds(4)
        # type the the first text:
        m.keys.press("<tab>")
        m.keys.type("The script will resume until you hit ENTER<enter>")
        m.keys.type("Try it and type something and when your done hit the ENTER key.<enter>")
        # wait until the ENTER key is pressed
        m.wait.for_key_press("<enter>")
        # coordinates for the target to click in
        top_left = (0, 0)
        bottom_right = (500, 500)
        # type message and tell where to click
        m.keys.type("Well done, now the script will pause until you clicked " \
                    "with the left mouse button on the top-left part of your " \
                    "screen in the rectangle ({}x{})<enter>".format(top_left, bottom_right))
        # wait for the mouse click until it hits the target
        coord = m.wait.for_mouse_click(top_left, bottom_right)
        # message the clicked coordinates for 3 seconds
        sg.PopupNoButtons(f"You clicked on: {coord}",
                          title="Position",
                          auto_close_duration=3,
                          auto_close=True,
                          font=("Calibri", 14)
                          )
        # exit full screen
        m.keys.press("<f11>")


if __name__ == "__main__":
    main()
