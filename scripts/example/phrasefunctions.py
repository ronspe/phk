import re
from app.script import phrasefunction

"""
=============== Examples of Phrase functions =====================
What is a Phrase function?
This is a function that is used to expand text just like the regular phrases but
it is much more flexible then "If I type this exact string: then expand to ..."

For PHK this is a black box. It inputs a text into a function and expects a list back.
Everything inside the function is up to you, e.g. you can use regular expressions, add
exceptions to the case, fuzzy matching of text, etc.. 
This gives full control over text expansion.

Rules of a Phrase function:
- PHK will recognise a "Phrase function" by the decorator: @phrasefunction().
  - The decorator can have a priority parameter. The default value is 100.
    - This parameter is used in case more then one Phrase function will return a result.
    - The function with the highest prio will win and only this function will be 
      executed.
- The script should be located in a "/scripts" sub-folder
- The Phrase function has only one parameter named: "text"
- The output must be list with two string values:
    [text_to_remove, text_to_type]
    e.g.: return ["this", "that"]
- If there is no result the Phrase function must return an empty list.
- If there is a normal Phrase defined that will also match the trigger, only the 
  normal Phrase will be expanded. The Phrase function will not be executed to avoid
  a possible chain reaction on the already expanded text.
  
Order of execution:
- If there is a Phrase that match the trigger: ONLY the Phrase will expand.
- If there is no Phrase that match the trigger: Check if there is a Phrase function 
  that does.
  - If there is one Phrase function that matches: execute it.
  _ If there are more then one Phrase function that matches: execute only the one
    with the highest prio.
    
See below for some examples.
"""

@phrasefunction()
def remove_consecutive_duplicate_words(text: str) -> list:
    """
    This is a "phrase" function that is used to expand text just like the regular phrases.
    This function will remove the second word if the same word is typed twice.
    The regex used is case-sensitive.
    e.g.: the the -> the
    There is no prio set in the decorator so the prio = 100 (highest prio)
    :param text:
    :return: a list with the text to remove and the text to replace
    """
    result = []
    # PHK will replace a space: " " with "<space>", this is not very convenient in a
    # regular expression so replace it for a " ".
    text = text.replace("<space>", " ")
    pattern = r"(\b\S+\b)\s+\b\1\b\s+"   # Test it on: https://regex101.com/
    match = re.search(pattern, text)
    if match:
        last_double_word = match.group(1)
        text_to_remove = f" {last_double_word}"  # Also added a space to remove
        text_to_type = ""  # No need to type anything, we just remove text
        result = [text_to_remove, text_to_type]
    return result

@phrasefunction()
def correct_consecutive_uppercase_characters(text: str) -> list:
    """
    This is a "phrase" function that is used to expand text just like the regular phrases.
    This function will correct two uppercase characters in the beginning of a word.
    The regex used is case-sensitive.
    There has to be a space before the word.
    e.g.: " TH" -> " Th"
    There is no prio set in the decorator so the prio = 100 (highest prio)
    :param text:
    :return: a list with the text to remove and the text to replace
    """
    result = []
    # PHK will replace a space: " " with "<space>", this is not very convenient in a
    # regular expression so replace it for a " ".
    text = text.replace("<space>", " ")
    # PHK will also register the <shift> key in the recorded text, we need to remove
    # this too.
    text = text.replace("<shift>", "")
    pattern = r" [A-Z]{2}"   # Test it on: https://regex101.com/
    match = re.search(pattern, text)
    if match:
        last_character = match.group(0)[-1]
        text_to_remove = f"{last_character}"
        text_to_type = last_character.lower()
        result = [text_to_remove, text_to_type]
    return result

def fuzzy_match_list(text: str, alist: list) -> str:
    """
    This is a helper function to fuzzy filter a list until
    there is ONLY ONE possible match.
    :param text: The text that we can use as a pattern to search
    :param alist: A list of strings that we can check. 
    :return: a single string 
    """
    result = None
    def fuzzy_find(text, alist):
        results = []
        pattern1 = ".*?".join(map(re.escape, text))
        pattern2 = f"(?=({pattern1}))"
        regex = re.compile(pattern2, re.IGNORECASE)
        l = lambda x: x
        for item in alist:
            rl = list(regex.finditer(l(item)))
            if rl:
                sm = min(rl, key=lambda x: len(x.group(1)))
                results.append((len(sm.group(1)), sm.start(), l(item), item))
        return (z[-1] for z in sorted(results))

    items = fuzzy_find(text, alist)
    match = [x for x in items]
    if len(match) == 1:
        result = match[0]
    return result

# A list of cities used for expansion in the function below.
cities = ["Alexandria", "Baton", "Bellingham", "Birmingham", "Bridgeport", "Burlington",
          "Carson", "Cedar", "Charleston", "Cincinnati", "Cumberland", "Evansville",
          "Grand", "Grant", "Greensboro", "Greenville", "Hagerstown", "Harrisburg",
          "Huntington", "Huntsville", "Idaho", "Jersey City"]

@phrasefunction(prio=50)
def fuzzy_match_cities(text: str) -> list:
    """
    --- Goal of this example function ---
    I want to trigger a text expansion to expand to a city name located
    in the list "cities".
    The trigger should fuzzy match the items in the list.
    e.g.: when I type: 
        "<space>port" should expand to "Bridgeport" 
        "<space>green" should not expand because it has two possible matches.
        "<space>greeno" should expand to "Greensboro"
    I want to prevent false matches as much as possible.
    e.g: I could type "<space>green" in a text as a color name and not the city.
    One way to do this is:
        - I use two spaces to trigger that I want to expand the next word
          that I will type.
        - The text must be longer then 3 characters, less characters is not
          practical.
    This example gave the prio 50 to this Phrase function.
    """
    result = []  # This is the result when there is no match
    trigger = "<space><space>"
    if trigger in text:
        # find the index of the last occurrence of the trigger in the text
        trigger_index = text.rfind(trigger) + len(trigger)
        # remove the trigger from the text and all characters in front of it,
        # leaving only the text we want to check
        text = text[trigger_index:]
        if len(text) > 3:
            city = fuzzy_match_list(text, cities)
            if city is not None:
                # I add a space to the text, because I triggered this by using
                # two spaces, I need to remove the last one too.
                text_to_remove = f" {text}"
                text_to_type = city
                result = [text_to_remove, text_to_type]
    return result

