# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

from app.script import Expand

phrases = [
    [Expand, "<space>btw<space>", " by the way ", "btw"],
    [Expand, "<space>kr<enter>", "Kind regards,<enter>John", "kind regards"],
    [Expand, "mymail", "mymail@yahoo.com", "mymailYahoo"],
    [Expand, "mymail", "othermail@yahoo.com", "myothermailYahoo"],
]

"""
The last phrase is a duplicate phrase! 
It will overwrite the previous phrase "mymail" phrase in this 
file. But also when the same phrase is previously loaded from another file.

The 'default' folder will be loaded first. In this folder all files 
are loaded alphabetically, the last files will overwrite the
previous phrases if duplicates occur.
Use the default folder for scripts and phrases that you always need.

Then the folder you provided as a commandline parameter is loaded. 
In this folder all files are also loaded alphabetically.
You can use this behaviour to overwrite the default scripts and phrases
to load project specific phrases or load different hotkeys 
based on your current OS and overwrite the default ones.
"""
