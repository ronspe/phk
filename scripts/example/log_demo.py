# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

# Use pySimpleGUI for the popup window
import PySimpleGUI as sg
sg.theme("LightBlue3")
# More themes: https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion

THIS_FILE = this_file(__file__)
hotkeys = []

hotkeys.append([RunPython, "<ctrl><alt>4", THIS_FILE, "Logging demo"])


def main():
    x = m.mouse.position_x()
    y = m.mouse.position_y()
    m.log.info(f"Mouse position: x={x} y={y}")
    m.mouse.move.left(5)
    m.mouse.move.up(5)
    m.log.info(f"Mouse position: x={m.mouse.position()}")
    m.system.python_version()
    m.log.debug("Should NOT see this message on screen, ONLY in macro.log")
    m.log.error("You should see this on screen AND in macro.log")
    m.log.critical("You should see this on screen AND in macro.log")
    sg.PopupOK('Please check scripts/macro.log', title='Done')



if __name__ == "__main__":
    main()
