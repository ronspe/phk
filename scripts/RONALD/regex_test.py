import re
# https://regex101.com/
from app.script import phrasefunction


@phrasefunction()
def phrasefunction_remove_consecutive_duplicate_words(text: str) -> list:
    """
    This is a "phrase" function that is used to expand text like the regular phrases.
    This will remove the second word if the same word is typed twice.
    e.g.: the the -> the
    :param text:
    :return: cleaned string
    """
    result = []
    pattern = r"\b(\w+)\s+\1\b"
    flags = re.IGNORECASE
    match = re.search(pattern, text, flags=flags)
    if match:
        last_double_word = match.group(1)
        # result = text.replace(first_double_word, "", 1)
        text_to_type = ""  # No need to type anything, we just remove text
        text_to_remove = f" {last_double_word}"  # Also added a space to remove
        result = [text_to_remove, text_to_type]
    return result


def fuzzy_match_list(text: str, alist: list) -> str:
    """
    This is a helper function to fuzzy filter a list until there is
    only one possible match.
    :param text: The text that we can use as a pattern to search
    :param alist: A list of strings that we can check. 
    :return: a single string 
    """
    result = None
    def fuzzy_find(text, alist):
        results = []
        pattern1 = ".*?".join(map(re.escape, text))
        pattern2 = f"(?=({pattern1}))"
        regex = re.compile(pattern2, re.IGNORECASE)
        l = lambda x: x
        for item in alist:
            rl = list(regex.finditer(l(item)))
            if rl:
                sm = min(rl, key=lambda x: len(x.group(1)))
                results.append((len(sm.group(1)), sm.start(), l(item), item))
        return (z[-1] for z in sorted(results))

    items = fuzzy_find(text, alist)
    match = [x for x in items]
    if len(match) == 1:
        result = match[0]
    return result

cities = ["Alexandria", "Baton", "Bellingham", "Birmingham", "Bridgeport", "Burlington",
          "Carson", "Cedar", "Charleston", "Cincinnati", "Cumberland", "Evansville",
          "Grand", "Grant", "Greensboro", "Greenville", "Hagerstown", "Harrisburg",
          "Huntington", "Huntsville", "Idaho", "Jersey City"]

@phrasefunction()
def phrasefunction_fuzzy_match_cities(text: str) -> list:
    """
    --- What is a Phrase function? ---
    This is a "phrase" function that is used to expand text like the regular phrases.
    This gives full control over text expansion.
    PHK will recognise this as a "phrase" function by it's name.
    It needs to follow these rules:
        - It should be located in a "scripts" sub-folder
        - The name of the function must start with: "phrasefunction_"
        - There is only one parameter named: "text"
        - The output must be list with two string values:
            [text_to_remove, text_to_type]
            e.g.: return ["etc", "etcetera"]
        - If there is no result we return an empty list

    --- Goal of this example function ---
    I want to trigger a text expansion to expand to a city name located
    in the list "cities".
    The trigger should fuzzy match the items in the list.
    e.g.: when I type: 
        "<space>port" should expand to "Bridgeport" 
        "<space>green" should not expand because it has two possible matches.
        "<space>greenv" should expand to "Greenville"
    I want to prevent false matches as much as possible.
    e.g: I could type "<space>green" in a text as a color name and not the city.
    One way to do this is:
        - I use two spaces to trigger that I want to expand the next word
          that I will type.
        - The text must be longer then 3 characters, less characters is not
          practical.
    """
    result = []  # This is the result when there is no match

    if text.startswith("  "):
        text = text.replace(" ", "", 2)  # remove two space characters
        if len(text) > 3:
            city = fuzzy_match_list(text, cities)
            if city is not None:
                text_to_type = city
                # I add a space to the text, because I triggered this by using
                # two spaces, I need to remove the last one too.
                text_to_remove = f" {text}"
                result = [text_to_remove, text_to_type]
    return result

if __name__ == "__main__":
    # THE TYPED TEXT
    text = "is is"
    # INPUT IN SCRIPT
    print(phrasefunction_remove_consecutive_duplicate_words(text))
    text = "  alex"
    print(phrasefunction_fuzzy_match_cities(text))
