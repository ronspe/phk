# -*- coding: utf-8 -*-
from app.script import Expand

phrases = [
    # Audi
    [Expand, ";cms", "K@rol9na0dCykas", "AEM Live"],
    [Expand, ";ure", "@t4Mw18UtM9", "Uren registrati"],
    [Expand, ";sit", "z5/MTm9B|888gB", "Sitecore"],
    [Expand, ";vpn", "QWaspon7!", "Sitecore VPN"],
    # Emakina
    # [Expand, ";ema", "zz6&bv&W", "Emakina"],
    # [Expand, ";dem", "KWasomr3", "Omron Demandware"],
    # [Expand, ";wor", "qXVR86)hAIJqyygG3F$qP2h*", "Omron WP"],
    # Mail
    [Expand, ";mro", "!erm@nbr00d", "Ronald"],
    [Expand, ";<space>test<space>", "test", "Test spaties"],
]
