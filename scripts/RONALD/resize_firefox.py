# -*- coding: utf-8 -*-
import argparse
from app.script import RunPython, this_file
from scripts.RONALD.x11window import X11Window

THIS_FILE = this_file(__file__)
hotkeys = []
# resize to 1400x900: maat Mac.
cmd = "{} Firefox 1400 983".format(THIS_FILE)  # 83 bij de hoogte voor window header
hotkeys.append([RunPython, "<ctrl><shift>1", cmd, 'resize FF to Mac'])

#resize to 1366x768: is meeste gebruikt formaat volgens W3schools
cmd = "{} Firefox 1366 851".format(THIS_FILE)  # tel 83 op bij de hoogte
hotkeys.append([RunPython, "<ctrl><shift>2", cmd, 'resize FF to popular'])

# TODO: resize werkt niet meer...

def resize_window(name: str, width: int, height: int) -> None:
    xwin = X11Window()
    xwin.set(name)
    xwin.resize(width, height)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("name")
    parser.add_argument("width", type=int)
    parser.add_argument("height", type=int)
    args = parser.parse_args()
    resize_window(args.name, args.width, args.height)
