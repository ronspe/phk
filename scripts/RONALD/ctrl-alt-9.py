import app.macro as m  # The library with 'macro' commands.

from app.script import RunPython, this_file
from scripts.RONALD.lib.firefox import Firefox
from scripts.RONALD.lib.copyq import CopyQ

THIS_FILE = this_file(__file__)
hotkeys = []
hotkeys.append([RunPython, "<ctrl><alt>9", THIS_FILE, "Drupal alternate urls"])


ff = Firefox()
copyq = CopyQ()

"""
Lattiz:Drupal: add the recipes page category
"""

def main():


    # ff.click_link("meta")
    #
    # m.wait.seconds(1)
    # ff.scroll_to_bottom()
    # ff.click_link("alternative")
    # ff.click_link("x")

    # x-default
    copyq.paste_number("6")
    ff.tab()
    # DE
    copyq.paste_number("4")
    ff.tab()
    # EN
    copyq.paste_number("6")
    ff.tab()
    # ES
    copyq.paste_number("5")
    ff.tab()
    # FR
    copyq.paste_number("3")
    ff.tab()
    # BE FR
    copyq.paste_number("0")
    ff.tab()
    # NL
    copyq.paste_number("2")
    ff.tab()
    # BE NL
    copyq.paste_number("1")
    m.keys.press("<esc>")
    ff.scroll_to_top()
    m.keys.press("<space>")
    #ff.scroll_down_half_page(4, wait=0.5)


if __name__ == "__main__":
    main()
