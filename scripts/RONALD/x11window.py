# -*- coding: utf-8 -*-
import app.macro as m
import re
import sys


class X11Window:
    def __init__(self):
        if not m.shell.executable_found("xprop") and not m.shell.executable_found(
            "wmctrl"
        ):
            sys.exit("Aborting, please install: 'wmctrl' and 'xprop'.")
        self.active_window = self._get_active_window()

    def _get_active_window(self):
        aw = m.shell.return_output("xprop -root _NET_ACTIVE_WINDOW")
        match = re.search("(^.*\# )(.*)(,.*$)", aw)
        aw_id = match.group(2)
        active_window = self._get_window(aw_id[-7:])
        return active_window

    def set(self, unique_string):
        w = self._get_window(unique_string)
        self.unique_string = unique_string
        try:
            self.id = w["id"]
            self.short_id = w["short_id"]
            self.desktop_id = w["desktop_id"]
            self.pid = w["pid"]
            self.class_name = w["class_name"]
            self.client_machine = w["client_machine"]
            self.title = w["title"]
        except KeyError:
            sys.exit("Window not found: [{}]".format(unique_string))

    @staticmethod
    def _get_window(unique_string):
        windows = m.shell.return_output("wmctrl -lxp")
        w = {}
        for window in windows.split("\n"):
            if unique_string in window:
                w["id"] = window.split()[0]
                w["short_id"] = window.split()[0][-7:]
                w["desktop_id"] = window.split()[1]
                w["pid"] = window.split()[2]
                w["class_name"] = window.split()[3]
                w["client_machine"] = window.split()[4]
                w["title"] = " ".join(window.split()[5:-1])
                break
        m.log.debug(w)
        return w

    def reset(self):
        cmd = "wmctrl -ir {} -b remove,maximized_vert,maximized_horz,below,shaded".format(self.id)
        m.shell.run_command(cmd)

    def unmax(self):
        cmd = "wmctrl -ir {} -b remove,maximized_vert,maximized_horz".format(self.id)
        m.shell.run_command(cmd)

    def maximize(self):
        self.reset()
        cmd = "wmctrl -ir {} -b add,maximized_vert,maximized_horz".format(self.id)
        m.shell.run_command(cmd)

    def resize(self, width: int, height: int) -> None:
        cmd = "xdotool windowsize $(xdotool search --name {} | tail -1) {} {}".format(
            self.unique_string, width, height)
        self.reset()
        m.shell.run_command(cmd)
