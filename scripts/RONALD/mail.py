# -*- coding: utf-8 -*-
from app.script import Expand

phrases = [
    # Mail addressen
    [Expand, ";mmc", "contentmanagement@moinne.com", "cm@moinne"],
    [Expand, ";mmr", "ronald.speelman@moinne.com", "rs@moine"],
    [Expand, ";mgr", "ronald.speelman@gmail.com", "rs@gmail"],
    # Mail phrases
    [Expand, ";gr", "Groeten, Ronald", "Groeten"],
    [Expand, ";tr", "Thanks, Ronald", "Thanks"],
    [Expand, ";kr", "Kind regards, Ronald", "Kind regards"],
]
