# -*- coding: utf-8 -*-


import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

#     place this script in the default folder
#     Run this script so the virtual environment is active:
#     .venv/bin/python3 scripts/default/test_santiago.py

# the output shows your input path
# when the virtual environment is active it should show something like:
# /data/gitlab/phk/.venv/lib/python3.6/site-packages
print('\n'.join(sys.path))

# below we try to do some imports and see which one is failing
imports_succes = True

try:
    import app
except ImportError:
    imports_succes = False
    print("cannot import app")

try:
    import app.macro
except ImportError:
    imports_succes = False
    print("cannot import app.macro")

try:
    from app.script import RunPython, this_file
except ImportError:
    imports_succes = False
    print("cannot import app.script")

try:
    import app.phk.recorder as recorder
except ImportError:
    imports_succes = False
    print("cannot import app.phk.recorder")


def main():
    if imports_succes:
        print("All imports are working")

if __name__ == "__main__":
    main()
