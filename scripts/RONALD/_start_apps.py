# -*- coding: utf-8 -*-
import os
from app.script import Run
hivedir = os.environ.get('HIVEDIR')
homedir = os.environ.get('HOME')

hotkeys = []

hotkeys.append([Run, "<shift><cmd>v", 'gvim -c "startinsert"&', "Vim scratch"])
hotkeys.append([Run, "<shift><cmd>n", "xfce4-popup-notes&", "Notes"])
hotkeys.append(
    [
        Run,
        "<shift><cmd>e",
        f"{hivedir}/usb-nood/encryptpad/encryptpad0_4_0_4.AppImage&",
        "Encryptpad",
    ]
)
hotkeys.append(
    [
        Run,
        "<shift><cmd>p",
        f"bash -c 'cd {homedir}/pycharm-community-2020.1.2/bin; ./pycharm.sh'&",
        "Pycharm",
    ]
)
