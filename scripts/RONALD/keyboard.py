# -*- coding: utf-8 -*-
import os
from app.script import Run

gitdir = os.environ.get('GITDIR')
hotkeys = []
hotkeys.append([Run, "<alt><cmd><f1>", f"bash -c '. \"{gitdir}/shellscripts/toggle-touchpad.sh\"'", "Toggle touchpad"])
hotkeys.append([Run, "<alt><cmd><f2>", f"bash -c '. \"{gitdir}/shellscripts/toggle-keyboard.sh\"'", "Toggle keyboard"])
