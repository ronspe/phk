# -*- coding: utf-8 -*-
import random
import app.macro as m  # The library with 'macro' commands.
from app.script import RunPython, this_file

THIS_FILE = this_file(__file__)
hotkeys = []

hotkeys.append([RunPython, "<ctrl>`", THIS_FILE, "Right mouse-click"])

"""
The script will use a mouse right-click with a keyboard shortcut:
ctrl backtick

"""


def main():
    m.wait.seconds(0.5)
    m.mouse.click.right()

if __name__ == "__main__":
    main()
