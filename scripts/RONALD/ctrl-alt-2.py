import app.macro as m  # The library with 'macro' commands.

from app.script import RunPython, this_file
from scripts.RONALD.lib.firefox import Firefox
from scripts.RONALD.lib.copyq import CopyQ

THIS_FILE = this_file(__file__)
hotkeys = []
hotkeys.append([RunPython, "<ctrl><alt>2", THIS_FILE, "Voorraaddeals paste"])


ff = Firefox()
copyq = CopyQ()

"""
Lattiz:Drupal: add the recipes page category
"""

def main():


    # zet eerst de muis op het juiste component

    # zoek de juiste image
    m.keys.press("<ctrl>f")
    m.wait.seconds(1)
    copyq.paste_number("1")  # komnr
    m.mouse.click.left(clicks=2)  # dubble click op het component
    # copieer de juiste image van het model
    # klik op het woord "image"
    m.wait.for_key_press("<space>")
    m.wait.seconds(1)
    # ga naar modelnaame en maak het eerst leeg
    m.keys.press("<tab>", times=6)
    m.keys.press("<backspace>", times=45)
    copyq.paste_number("2")  # modelnaam
    m.wait.seconds(0.5)
    m.keys.press("<tab>")
    m.wait.seconds(0.5)
    m.keys.press("<ctrl><home>") # selecteer de oude tekst
    m.wait.seconds(0.5)
    m.keys.press("<shift><page_down>")
    m.wait.seconds(0.5)
    m.keys.press("<delete>")  # haal de oude tekst weg
    m.wait.seconds(0.5)
    copyq.paste_number("3")  # omschrijving
    m.wait.seconds(0.5)
    m.keys.press("<backspace>")  # haal de quotes weg
    m.keys.press("<ctrl><home>")
    m.wait.seconds(0.5)
    m.keys.press("<delete>")
    m.wait.seconds(0.5)
    m.keys.press("<tab>", times=1)
    copyq.paste_number("4")  # link tekst
    m.keys.press("<tab>", times=1)
    copyq.paste_number("5")  # url
    m.wait.seconds(0.5)
    m.keys.press("<tab>", times=2)
    m.keys.press("<enter>")  # click OK
    # scroll down a little
    m.wait.seconds(0.5)
    m.keys.press("<down>", times=5)


if __name__ == "__main__":
    main()
