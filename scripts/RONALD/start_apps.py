# -*- coding: utf-8 -*-
from app.script import Run

hotkeys = []

hotkeys.append([Run, "<shift><cmd>v", 'gvim -c "startinsert"&', "Vim scratch"])
hotkeys.append([Run, "<shift><cmd>n", "xfce4-popup-notes&", "Notes"])
hotkeys.append(
    [
        Run,
        "<shift><cmd>e",
        "/data/spideroak/usb-nood/encryptpad/encryptpad0_4_0_4.AppImage&",
        "Encryptpad",
    ]
)
hotkeys.append(
    [
        Run,
        "<shift><cmd>p",
        "bash -c 'cd /home/ronald/pycharm-community-2019.2.3/bin; ./pycharm.sh'&",
        "Pycharm",
    ]
)
