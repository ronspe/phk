import re
# https://regex101.com/

def remove_consecutive_duplicate_words(text: str) -> str:
    """
    This will remove the second word if the same word is typed twice.
    e.g.: the the -> the
    :param text:
    :return: cleaned string
    """
    pattern = r"\b(\w+)\s+\1\b"
    flags = re.IGNORECASE
    match = re.search(pattern, text, flags=flags)
    if match:
        first_double_word = match.group(1)
        result = text.replace(first_double_word, "", 1)
        return result
    else:
        return None



def fuzzy_match_list(text: str, alist: list) -> str:
    """
    Filter self.menu with fuzzy search
    :return: A filtered and sorted list.
    """
    result = None
    def fuzzy_find(text, alist):
        results = []
        pattern1 = ".*?".join(map(re.escape, text))
        pattern2 = f"(?=({pattern1}))"
        regex = re.compile(pattern2, re.IGNORECASE)
        l = lambda x: x
        for item in alist:
            rl = list(regex.finditer(l(item)))
            if rl:
                sm = min(rl, key=lambda x: len(x.group(1)))
                results.append((len(sm.group(1)), sm.start(), l(item), item))
        return (z[-1] for z in sorted(results))

    items = fuzzy_find(text, alist)
    match = [x for x in items]
    if len(match) == 1:
        result = match[0]
    return result

if __name__ == "__main__":
    # THE TYPED TEXT
    text = "is is"
    # INPUT IN SCRIPT
    print(remove_consecutive_duplicate_words(text))
    text = "baon"
    cities = ["Alexandria", "Baton", "Bellingham", "Birmingham", "Bridgeport", "Burlington",
              "Carson", "Cedar", "Charleston", "Cincinnati", "Cumberland", "Evansville",
              "Grand", "Grant", "Greensboro", "Greenville", "Hagerstown", "Harrisburg",
              "Huntington", "Huntsville", "Idaho", "Jersey City"]
    print(fuzzy_match_list(text, cities))
