import app.macro as m  # The library with 'macro' commands.

from app.script import RunPython, this_file
from scripts.RONALD.lib.firefox import Firefox
from scripts.RONALD.lib.copyq import CopyQ

THIS_FILE = this_file(__file__)
hotkeys = []
hotkeys.append([RunPython, "<ctrl><alt>1", THIS_FILE, "Voorraaddeals copy"])


ff = Firefox()
copyq = CopyQ()

"""
Lattiz:Drupal: add the recipes page category
"""

def main():

    # dit is een hack, anders maakt hij de cell leeg
    #m.keys.press("<left>")
    #m.wait.seconds(1)
    #m.keys.press("<right>")
    #m.wait.seconds(1)
    #m.keys.press("<ctrl>z")
    #m.wait.seconds(1)
    # copy url
    m.keys.press("<ctrl>c")
    m.keys.press("<left>")
    m.wait.seconds(0.5)

    # copy linktext
    m.keys.press("<ctrl>c")
    m.keys.press("<left>")
    m.wait.seconds(0.5)

    # copy text
    m.keys.press("<ctrl>c")
    m.keys.press("<left>")
    m.wait.seconds(0.5)

    # copy headline
    m.keys.press("<ctrl>c")
    m.keys.press("<left>")
    m.wait.seconds(0.5)

    # copy komnr
    m.keys.press("<ctrl>c")
    m.keys.press("<ctrl>b")  # markeer de row als done
    m.wait.seconds(0.5)

    # row naar beneden
    m.keys.press("<down>")
    m.wait.seconds(0.5)
    m.keys.press("<right>")
    m.keys.press("<right>")
    m.keys.press("<right>")
    m.keys.press("<right>")

if __name__ == "__main__":
    main()
