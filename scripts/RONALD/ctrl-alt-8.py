import app.macro as m  # The library with 'macro' commands.

from app.script import RunPython, this_file
from scripts.RONALD.lib.firefox import Firefox
from scripts.RONALD.lib.copyq import CopyQ

THIS_FILE = this_file(__file__)
hotkeys = []
hotkeys.append([RunPython, "<ctrl><alt>8", THIS_FILE, "Audi Publish"])

ff = Firefox()
copyq = CopyQ()

"""
"""

def main():
    ff.click_link("Publish")
    m.keys.press("<down>")
    m.keys.press("<enter>")
    m.wait.seconds(8)
    # There are now 2 Publish buttons on the page, I need the second one
    ff.click_link("Publish<tab>", pause=1)
   

if __name__ == "__main__":
    main()
