# -*- coding: utf-8 -*-
from app.script import Expand, RunPython, this_folder

THIS_FOLDER = this_folder(__file__)

phrases = [
    # HTML prefix = ,
    [Expand, ",br", "<br />", "html <br>"],
    [Expand, ",p", "<p></p><left><left><left><left>", "html <p>"],
    [Expand, ",h1", "<h1></h1><left><left><left><left><left>", "html <h1>"],
    [Expand, ",h2", "<h1></h1><left><left><left><left><left>", "html <h2>"],
    [Expand, ",h3", "<h1></h1><left><left><left><left><left>", "html <h3>"],
    [Expand, ",h4", "<h1></h1><left><left><left><left><left>", "html <h4>"],
    [Expand, ",nb", "&nbsp;", "html &nbsp;"],
]

