# -*- coding: utf-8 -*-

import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..")))

from app.script import Run

hotkeys = []

hotkeys.append([Run, "<shift><cmd>c", "open -a 'Google Chrome'", "Chrome"])
hotkeys.append([Run, "<shift><cmd>f", "open ~", "Finder"])
hotkeys.append([Run, "<shift><cmd>o", "open -a LibreOffice", "Office"])
hotkeys.append([Run, "<shift><cmd>t", "open -a iTerm", "Terminal"])
hotkeys.append([Run, "<shift><cmd>v", "open -a Vim", "Vim"])
hotkeys.append([Run, "<shift><cmd>w", "open -a Firefox", "Firefox"])

