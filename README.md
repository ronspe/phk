What is this?
===============
PythonHotKey is a Python application that can be used to emulate keystrokes, move and click the mouse, launch programs, and open documents.  
These actions can be triggered by hotkeys like [F7] or [ctrl + 4] or by a typed phrase.  
PythonHotKey (PHK) tries to be an alternative for AutoHotKey on Linux and MacOS.  

PHK is build on top of the PyAutoGUI and Pynput packages.  
PHK is currently only tested on Linux (Xubuntu) with Python 3.6 and limited tested on MacOS with Python 3.7.

The PHK [macro-scripts](/doc/macro_documentation.md) can be written in pure Python.  
There is a [macro library](/doc/macro_documentation.md#macro-functions) that makes writing the macros very easy.  
There is even a [macro recorde](/doc/macro_documentation.md#macro-recorder)r that can do most of the work for you.  

**[View the documentation](/doc/index.md)**
