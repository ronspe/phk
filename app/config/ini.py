# -*- coding: utf-8 -*-
import os
import app
import json
import types
import configparser
import app.config as config
from typing import Any

log = config.default_logger

"""
Fallback values for items that can be managed in the config.ini file.
Even if someone deletes a value in the config.ini, a fallback is provided
so the application will not fail.
"""
fallback_dtm = types.SimpleNamespace()
fallback_dtm.key = "<capslock>"
fallback_dtm.width = 40
fallback_dtm.height = 10
fallback_dtm.pos_x = 100
fallback_dtm.pos_y = 100
fallback_dtm.theme = "DarkGrey6"

fallback_recorder = types.SimpleNamespace()
fallback_recorder.pos_x = 50
fallback_recorder.pos_y = 50
fallback_recorder.message_timeout = 10

fallback_phrases = types.SimpleNamespace()
fallback_phrases.type_interval = 0.05

fallback_hotkeys = types.SimpleNamespace()
fallback_hotkeys.type_interval = 0.05

# If one of these folders is used as a context, the default folder will not be read.
fallback_no_default_folders = ["TEST", "test", "example"]


class Ini:

    DEFAULT_SECTION = "default"

    def __init__(self) -> None:
        """ Read the inifile, set first the values from the 'default' section,
            then read the values from the specific section,
            values are available as a variable. e.g. self.ini.logfile
        """
        self.cp = configparser.ConfigParser()
        self.section = app.user.context_folder
        self._read_ini_file()

    def _read_ini_file(self, ini_file: str = None) -> bool:
        found_file = False
        if ini_file is None:
            ini_file = os.path.abspath(
                os.path.join(
                    app.system.phk_run_path, app.user.root_folder, app.ini_file
                )
            )
        if os.path.isfile(ini_file):
            log.debug("Reading Ini file: {}".format(ini_file))
            found_file = True
            self.cp.read(ini_file)
        else:
            log.warning("Cannot read ini: {}".format(ini_file))
            print("WARNING: Cannot read ini: {}".format(ini_file))
            print("         Please restore the file from the PHK resources.")
            print("         Proceeding with the default application settings...")
        # We can still load the needed values even if the ini file is
        # removed because of the fallback values in this file.
        return found_file

    def read_string_value(self, name: str, fallback: str) -> str:
        """
        Read a string value from the ini file
        :param name:  Name of the item
        :param fallback:  The value if the item is not found
        :return: The string value
        """
        return self._read_ini_value(name, "string", fallback)

    def read_int_value(self, name: str, fallback: int) -> int:
        """
        Read an integer value from the ini file
        :param name:  Name of the item
        :param fallback:  The value if the item is not found
        :return: The integer value
        """
        return self._read_ini_value(name, "int", fallback)

    def read_float_value(self, name: str, fallback: float) -> float:
        """
        Read a decimal value from the ini file
        :param name:  Name of the item
        :param fallback:  The value if the item is not found
        :return: The decimal value
        """
        return self._read_ini_value(name, "float", fallback)

    def read_bool_value(self, name: str, fallback: bool) -> bool:
        """
        Read a boolean value from the ini file
        :param name:  Name of the item
        :param fallback:  The value if the item is not found
        :return: The boolean value
        """
        return self._read_ini_value(name, "bool", fallback)

    def read_list_values(self, name: str, fallback: list) -> list:
        """
        Read a list from the ini file
        :param name:  Name of the item
        :param fallback:  The value if the item is not found
        :return: The list
        """
        return self._read_ini_value(name, "list", fallback)

    def _read_ini_value(self, name: str, value_type: str, fallback: Any) -> Any:
        """
        Try to read the asked section/name combination in the ini file.
        If that combination does not exists, try the default section/name combination.
        If that fails, use the fallback value.
        :param name: Name of the item in the ini file
        :param value_type: The value type: string, int, float, bool or list
        :param fallback:  The value if no value can be found
        :return: The found value in the ini file
        """
        section = None
        if self.cp.has_option(self.section, name):
            section = self.section
        elif self.cp.has_option(self.DEFAULT_SECTION, name):
            section = self.DEFAULT_SECTION

        if section:
            result = self._read_section(section, name, value_type, fallback=fallback)
            log.debug(f"Ini section:[{section}]{name}={result}")
        else:
            result = fallback
            log.warning(f"Ini [{name}] not found. Using fallback value:[{result}]")
        return result

    def _read_section(
        self, section: str, name: str, value_type: str, fallback: Any
    ) -> Any:
        """
        Try to read the asked section/name combination in the ini file.
        :param name: Name of the item in the ini file
        :param value_type: The value type: string, int, float, bool or list
        :return: The found value in the ini file
        """
        try:
            if value_type == "string":
                result = self.cp.get(section, name)
            elif value_type == "int":
                result = self.cp.getint(section, name)
            elif value_type == "float":
                result = self.cp.getfloat(section, name)
            elif value_type == "bool":
                result = self.cp.getboolean(section, name)
            elif value_type == "list":
                # TODO: This only loads strings.
                # Use the decoder to load other types of data.
                result = json.loads(self.cp.get(section, name))
            else:
                log.error(
                    f"No type {value_type} know. Using fallback value: [{fallback}]"
                )
                result = fallback
        except ValueError:
            log.error(
                f"Wrong data in Ini:[{section}][{name}]. "
                f"Expecting a '{value_type}'. Using fallback value: [{fallback}]"
            )
            result = fallback
        return result
