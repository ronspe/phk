# -*- coding: utf-8 -*-
"""
The keyboard listener is responsible for:
- Listening to the keyboard
- Maintaining a list of active keys to detect a hotkey
- Maintaining a list of phrase keys
- Checking after each key press if a hotkey is pressed or a phrase is typed
"""
import app
import importlib
from datetime import datetime
from pynput.keyboard import KeyCode, Listener, Key
from app.phk.phrase import Phrase
# from app.phk.dtm import is_double_tapped, DtmMenu
from app.phk.dispatch import Dispatch
from app.phk.key_convert import KeyConvert
from app.config.ini import Ini, fallback_dtm
import app.config as config

from app.script.action import run_python
log = config.default_logger


class ListenerException(Exception):
    pass


class KeyboardListener:
    def __init__(self) -> None:
        self.listener: Listener = None
        self.active_listener: bool = False
        self.hotkeys: app.Actions = []  # List of hotkeys
        self.phrases: app.Actions = []  # List of phrases
        self.phrasefunctions = []  # List of phrasefunctions
        self.current_active_keys: set = set()  # The keys that are pressed NOW
        self.current_phrase_keys: app.KeyCodes = []  # List of keys stored for phrases
        self.dtm_key_code: str = None  # The key that will trigger the Double Tap Menu
        self.dtm_key_pressed: datetime = None  # Timestamp when the dtm key was pressed
        self.phrase_key_string: str = ""  # String made from current_phrase_keys
        self.key_convert = KeyConvert()  # Convert keys and key representation
        self.dispatch = Dispatch()  # Dispatch the actions for hotkeys and phrases
        self.current_phrasefunction_keys: app.KeyCodes = []  # List of keys stored for phrase functions
        self.phrasefunction_key_string: str = ""  # String made from current_phrasefunction_keys

    def start(self) -> None:
        """(Re)Start the keyboard listener"""
        log.debug("Listener started")
        self._get_dtm_key()  # This is not yet know when the class is intitialized...
        self.active_listener = True
        with Listener(
            on_press=self._on_press, on_release=self._on_release
        ) as self.listener:
            try:
                self.listener.join()
            except ListenerException as e:
                log.error(f"Key {e.args[0]} was pressed")

    def stop(self) -> None:
        """Stop the keyboard listener"""
        log.debug("Listener stopped")
        self.active_listener = False
        self.listener.stop()

    def activate(self) -> None:
        """(Re)activate the keyboard listener"""
        self.active_listener = True

    def deactivate(self) -> None:
        """Deactivate the listener"""
        self.active_listener = False

    def _on_press(self, key: KeyCode) -> None:
        """Check on every key press if there is a phrase or hotkey that
           should be triggered.
        """
        if self.active_listener:
            # log.debug(f"Key: [{key}]")
            self.deactivate()
            self._check_for_double_tap_menu(key)
            self._check_for_phrases(key)
            self._check_for_hotkeys(key)
            self.activate()

    def _on_release(self, key: Key) -> None:
        """Remove key from the active keys. Used only for hotkeys."""
        self._remove_current_active_key(key)

    def _remove_all_current_active_keys(self) -> None:
        """Remove all keys from the active keys. Used only for hotkeys."""
        log.debug("Remove all current_active_keys.")
        self.current_active_keys = set()

    def _remove_current_active_key(self, key: Key) -> None:
        """Remove key from the active keys. Used only for hotkeys."""
        try:
            # log.debug(f"Remove current_active_key: [{key}]")
            self.current_active_keys.remove(key)
            # log.debug(f"Current_active_keys:{self.current_active_keys}")
        except KeyError:
            pass

    def _add_current_active_key(self, key: Key) -> None:
        """Add keys from the active keys. Used only for hotkeys."""
        try:
            # log.debug("Add current_active_key: [{}]".format(key))
            self.current_active_keys.add(key)
            # log.debug("Current_active_keys:{}".format(self.current_active_keys))
        except KeyError:
            pass

    def _check_for_hotkeys(self, key: KeyCode) -> None:
        """Check if current active keys should trigger a command."""
        # If the hotkey contains a <shift> that will register an uppercase.
        # key = Button().lowercase_char(key)
        key = self.key_convert.lowercase_char(key)
        for command in self.hotkeys:
            keys = command[3]
            try:
                if key in keys:
                    self._add_current_active_key(key)
                    if all(k in self.current_active_keys for k in keys):
                        # A hotkey is found.
                        self._remove_all_current_active_keys()
                        self.dispatch.actions(command)
            except TypeError:
                pass

    def _check_for_phrases(self, key: KeyCode) -> None:
        """Check if the typed string is present in the list of phrases."""
        phrase_string_found = False
        phrase_found = False
        ph = Phrase(self)
        phrase: app.ActionEntry = None
        ph.add_phrase_key_string(key)
        for phrase in self.phrases:
            phrase_string = phrase[1]
            if phrase_string.startswith(self.phrase_key_string):
                phrase_string_found = True
                if phrase_string == self.phrase_key_string:
                    phrase_found = True
                    log.debug(f"Phrase: [{phrase_string}] is found")
                    break

        if not phrase_string_found:
            ph.empty_current_phrase_keys()
        if phrase_found:
            self.dispatch.actions(phrase)
            ph.empty_current_phrase_keys()
        if not phrase_found:  # Only try phrasefunctions if there is no matching regular phrase
            phrase = self.execute_phrasefunctions()
            if len(phrase) > 0:
                self.dispatch.actions(phrase)
                ph.empty_current_phrasefunction_keys()

    def _check_for_double_tap_menu(self, key: KeyCode) -> None:
        """
        Check if the dtm key is double-tapped.
        If so: open the Double Tap Menu.
        """
        if key == self.dtm_key_code:
            if self.is_double_tapped(last_timestamp=self.dtm_key_pressed,
                                     max_secs_difference=0.25):
                self.active_listener = False
                run_python(app.system.phk_run_dtm_path)
            self.dtm_key_pressed = datetime.now()

    def _get_dtm_key(self):
        """Get the key for the Double Tap Menu"""
        dtm_key = Ini().read_string_value("dtm_key", fallback=fallback_dtm.key)
        self.dtm_key_code = self.key_convert.get_key_code(dtm_key)

    @staticmethod
    def is_double_tapped(last_timestamp: datetime.time,
                         max_secs_difference: float = 1) -> bool:
        """
        Checks the time difference between two key strokes.
        :param last_timestamp: The last time the key was hit.
        :param max_secs_difference: The maximum amount of seconds between two hits
               before we consider it a double tap.
        :return: True or False
        """
        double_tapped = False
        try:
            timedelta = datetime.now() - last_timestamp
            seconds = timedelta.days * 24 * 3600 + timedelta.seconds
            if seconds <= max_secs_difference:
                double_tapped = True
        except TypeError:  # Catches the None type for when it is first invoked
            pass
        return double_tapped

    def execute_phrasefunctions(self) -> list:
        """
        Executes phase functions.
        If more then one phrase function has a result only the one with the highes
        prio will be returned.
        :return: A list containing the Expand action to be used by the dispatcher.
        """
        results = []
        for item in self.phrasefunctions:
            import_path = item[0]
            phrasefunction = item[1]
            result = self.execute_phrasefunction(import_path, phrasefunction)
            if len(result) > 0:  # found a result, now make it an Expand action
                action = "Expand"
                trigger = result[0]  # text_to_remove
                value = result[1]  # text_to_type
                prio = result[2]  # needed in case more then one function has results
                results.append([action, trigger, value, prio])
        if len(results) > 0:
            # sort by prio, highest prio will be first in list
            sorted_results = sorted(results, key=lambda x: x[3], reverse=True)
            # return only the one with the highest prio
            first_item = sorted_results[0]
            # An Expand action only needs: Action, Trigger and Value
            # so remove the prio value from the list
            del first_item[-1]
            return first_item
        else:
            return []

                
    def execute_phrasefunction(self, import_path: str, phrasefunction: str) -> list:
        """
        Import and execute a phrasefunction
        :param import_path: the path used for importing the function
        :param phrasefunction: The name of the phrasefunction
        :param text: The text that is used as a parameter for the phrasefunction
        :return: The result of the phrasefunction: [text_to_remove, text_to_type]
        """
        result = []
        text = self.phrasefunction_key_string
        try:
            module = importlib.import_module(f"{import_path}")
        except ImportError:
            module = None
            log.error(f"Cannot import module: {module}")
        try:
            result = getattr(module, phrasefunction)(text)
            if len(result) > 0:
                log.debug(f"{phrasefunction}('{text})' -> {result[1]}")
            else:
                log.debug(f"{phrasefunction}('{text})' -> No results")

        except Exception as e:
            log.error(f"Module {module.__name__} is imported, cannot execute: {phrasefunction}")
            log.error(e)
        return result
