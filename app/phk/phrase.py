# -*- coding: utf-8 -*-
from typing import List, Tuple
from pynput.keyboard import KeyCode
import app.config as config
from app.phk.key_convert import KeyConvert
log = config.default_logger




class Phrase:
    """
    Functionality to expand a phrase with a replacement.
    The actual checking if a text needs to be expanded is done in keyboard_listener.py
    but the rest of the phrase functionality is done here.
    """

    def __init__(self, kl) -> None:
        self.kl = kl
        self.key_convert = KeyConvert()

    def register(self, action: str = None, trigger: str = None, replacement: str = None,
                 description: str=None) -> None:
        """
        Add a phrase to the phrases list located in keyboard_listener.py
        A phrase list is defined like this:
        [
        0 [action]
        1 [trigger]
        2 [replacement]
        3 []  # Empty, is now only used for hotkeys
        4 [description]
        ]
        :param action: Action can be of the types defined in app/__init__.py
        :param trigger:  A short phrase that will trigger auto expansion
        :param replacement: The text that will replace the phrase
        :param description: Optional description of the phrase
        """
        self.check_for_duplicates(trigger)
        log.debug(f"Adding phrase:{trigger}")
        self.kl.phrases.append([action, trigger, replacement, "", description])

    def register_functions(self, import_path: str = None, phrasefunction: str = None) -> None:
        """
        Add a phrasefunction to the phrasefunction list located in keyboard_listener.py
        :param import_path: The path that can be used to find the function. e.g. app.scripts.default
        :param phrasefunction: The name of the phrasefunction
        """
        log.debug(f"Adding phrasefunction: {import_path}.{phrasefunction}")
        self.kl.phrasefunctions.append([import_path, phrasefunction])

    def empty_current_phrase_keys(self) -> None:
        """
        Empty the current phrase keys list.
        Used in keyboard_listener.py
        """
        del self.kl.current_phrase_keys[:]
        self.kl.phrase_key_string = "".join(self.kl.current_phrase_keys)

    def empty_current_phrasefunction_keys(self) -> None:
        """
        Empty the current phrasefunction keys list.
        Used in keyboard_listener.py
        """
        del self.kl.current_phrasefunction_keys[:]
        self.kl.phrasefunction_key_string = "".join(self.kl.current_phrasefunction_keys)

    def add_phrase_key_string(self, key: KeyCode) -> None:
        """
        - converts a key to the corresponding key representation
        - add the key to the list of keys
        - make a string of the list of keys
        Used in keyboard_listener.py
        :param key: The keycode to add
        """
        try:
            key_string = self.key_convert.get_key_representation(key)
            self.kl.current_phrase_keys.append(key_string)
            self.kl.phrase_key_string = "".join(self.kl.current_phrase_keys)
            self.kl.current_phrasefunction_keys.append(key_string)
            self.kl.phrasefunction_key_string = "".join(self.kl.current_phrasefunction_keys)
        except TypeError:
            # The current phrase_keys are None, so the join will fail.
            pass

    def get_key_reference(self) -> str:
        """
        Reports available phrases.
        Used in phk.py
        """
        report = ''
        try:
            max_len = max(len(x[1]) for x in self.kl.phrases)
            if max_len < 7:
                max_len = 7
            formatting = "{:>" + str(max_len) + "} : {}\n"
            report += formatting.format('Trigger', 'Description')
            report += formatting.format('---', '-----------')
            items = []
            for phrase in self.kl.phrases:
                trigger = phrase[1]
                description = phrase[4]
                items.append(formatting.format(trigger, description))
            items.sort()
            for item in items:
                report += item
            header = f"{len(items)} Available phrases:\n"
        except ValueError:
            header = "No phrases available."
        return header + report

    def check_for_duplicates(self, trigger: str):
        """
        Checks for duplicate phrases.
        If the phrase is already present we remove it
        so we can overwrite it with a new one.
        Used in phk.py
        :param trigger: The phrase that needs to be checked.
        """
        duplicate_location = None
        for i in self.kl.phrases:
            duplicate = False
            trig = i[1]
            if trigger == trig:
                duplicate = True
                duplicate_location = self.kl.phrases.index(i)
                print(duplicate_location)
            if duplicate:  # overwrite the phrase
                log.warning(f"-- Duplicate phrase found:[{trigger}].")
                print(f"Warning: Duplicate phrase found:[{trigger}]."
                      f"Overwriting previous phrase.")
                del self.kl.phrases[duplicate_location]

