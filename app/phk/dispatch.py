# -*- coding: utf-8 -*-
import time
import app
import app.config as config
from pynput.keyboard import Key, Controller
from app.script.action import type_keys, run_python, run_shell_command, \
                              return_shell_command_output
from app.phk.key_convert import KeyConvert

log = config.default_logger


class Dispatch:

    def __init__(self) -> None:
        pass

    def actions(self, actions: app.ActionEntry) -> bool:
        """
        Execute an action given by a hotkey or phrase.
        The action performed is based on the name.
        Actions can be of the types defined in app/__init__.py
        :param actions: A list containing all actions.
        :return: True if an Action was performed, False if no action took place.
        """
        action_performed = True
        try:
            action = actions[0]  # Any action defined in app/__init__.py
            trigger = actions[1]  # A phrase of hotkey
            value = actions[2]  # A text, command or script

            self._remove_character(trigger)
            log.debug(f"Executing action: [{action}]")
            if action == app.action.type:
                log.debug(f"Typing string: [{value}]")
                interval = app.user.hotkey_type_interval
                type_keys(value, interval=interval)  # Type the replacement text
            elif action == app.action.expand:
                log.debug(f"Expanding to: [{value}]")
                interval = app.user.phrase_type_interval
                type_keys(value, interval=interval)  # Type the replacement text
            elif action == app.action.run:
                run_shell_command(value)
            elif action == app.action.python:
                run_python(value)
            elif action == app.action.return_output:
                output = return_shell_command_output(value)
                type_keys(output)
            else:
                self._message_wrong_action(action)
                action_performed = False
        except IndexError:
            self._message_missing_fields()
            action_performed = False
        return action_performed

    @staticmethod
    def _message_missing_fields():
        log.warning("Some required values are missing.")
        print("WARNING: Some required values are missing.")
        print("Make sure the action, trigger and the value are filled in."
              "The description is not required.")

    @staticmethod
    def _message_wrong_action(action):
        log.warning(f"Action: '{action}' not known.")
        print(f"WARNING: Action: '{action}' not known.")
        print(f"Please try any of the following actions:" 
              f"\n'{app.action.type}', '{app.action.run}', '{app.action.python}',"
              f"'{app.action.expand}' or '{app.action.return_output}'")

    @staticmethod
    def _remove_character(trigger: str) -> None:
        """
        Removes the chars that are typed as hotkey or phrase so they don't
        appear on the screen or in the text.
        :param trigger: The string containing the characters to remove
        """
        character_count = KeyConvert().count_characters(trigger)
        log.debug(f"Removing [{character_count}] characters with a backspace")
        time.sleep(0.05)
        keyboard = Controller()
        for i in range(0, character_count):
            keyboard.press(Key.backspace)
            keyboard.release(Key.backspace)

