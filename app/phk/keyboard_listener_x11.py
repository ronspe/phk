import sys
import os
from queue import Queue

# from je_auto_control.utils.exception.exception_tags import linux_import_error
#from je_auto_control.utils.exception.exception_tags import listener_error
# from je_auto_control.utils.exception.exceptions import AutoControlException

from Xlib.display import Display
from Xlib import X
from Xlib.ext import record
from Xlib.protocol import rq
from Xlib import XK
from threading import Thread

# get current display
current_display = Display()
display = Display(os.environ['DISPLAY'])


x11_linux_key_backspace = display.keysym_to_keycode(XK.string_to_keysym("BackSpace"))
x11_linux_key_slash_b = display.keysym_to_keycode(XK.string_to_keysym("BackSpace"))
x11_linux_key_tab = display.keysym_to_keycode(XK.string_to_keysym("Tab"))
x11_linux_key_enter = display.keysym_to_keycode(XK.string_to_keysym("Return"))
x11_linux_key_return = display.keysym_to_keycode(XK.string_to_keysym("Return"))
x11_linux_key_shift = display.keysym_to_keycode(XK.string_to_keysym("Shift_L"))
x11_linux_key_ctrl = display.keysym_to_keycode(XK.string_to_keysym("Control_L"))
x11_linux_key_alt = display.keysym_to_keycode(XK.string_to_keysym("Alt_L"))
x11_linux_key_pause = display.keysym_to_keycode(XK.string_to_keysym("Pause"))
x11_linux_key_capslock = display.keysym_to_keycode(XK.string_to_keysym("Caps_Lock"))
x11_linux_key_esc = display.keysym_to_keycode(XK.string_to_keysym("Escape"))
x11_linux_key_escape = display.keysym_to_keycode(XK.string_to_keysym("Escape"))
x11_linux_key_pgup = display.keysym_to_keycode(XK.string_to_keysym("Page_Up"))
x11_linux_key_pgdn = display.keysym_to_keycode(XK.string_to_keysym("Page_Down"))
x11_linux_key_pageup = display.keysym_to_keycode(XK.string_to_keysym("Page_Up"))
x11_linux_key_pagedown = display.keysym_to_keycode(XK.string_to_keysym("Page_Down"))
x11_linux_key_end = display.keysym_to_keycode(XK.string_to_keysym("End"))
x11_linux_key_home = display.keysym_to_keycode(XK.string_to_keysym("Home"))
x11_linux_key_left = display.keysym_to_keycode(XK.string_to_keysym("Left"))
x11_linux_key_up = display.keysym_to_keycode(XK.string_to_keysym("Up"))
x11_linux_key_right = display.keysym_to_keycode(XK.string_to_keysym("Right"))
x11_linux_key_down = display.keysym_to_keycode(XK.string_to_keysym("Down"))
x11_linux_key_select = display.keysym_to_keycode(XK.string_to_keysym("Select"))
x11_linux_key_print = display.keysym_to_keycode(XK.string_to_keysym("Print"))
x11_linux_key_execute = display.keysym_to_keycode(XK.string_to_keysym("Execute"))
x11_linux_key_prtsc = display.keysym_to_keycode(XK.string_to_keysym("Print"))
x11_linux_key_prtscr = display.keysym_to_keycode(XK.string_to_keysym("Print"))
x11_linux_key_prntscrn = display.keysym_to_keycode(XK.string_to_keysym("Print"))
x11_linux_key_printscreen = display.keysym_to_keycode(XK.string_to_keysym("Print"))
x11_linux_key_insert = display.keysym_to_keycode(XK.string_to_keysym("Insert"))
x11_linux_key_del = display.keysym_to_keycode(XK.string_to_keysym("Delete"))
x11_linux_key_delete = display.keysym_to_keycode(XK.string_to_keysym("Delete"))
x11_linux_key_help = display.keysym_to_keycode(XK.string_to_keysym("Help"))
x11_linux_key_win = display.keysym_to_keycode(XK.string_to_keysym("Super_L"))
x11_linux_key_winleft = display.keysym_to_keycode(XK.string_to_keysym("Super_L"))
x11_linux_key_winright = display.keysym_to_keycode(XK.string_to_keysym("Super_R"))
x11_linux_key_apps = display.keysym_to_keycode(XK.string_to_keysym("Menu"))
x11_linux_key_num0 = display.keysym_to_keycode(XK.string_to_keysym("KP_0"))
x11_linux_key_num1 = display.keysym_to_keycode(XK.string_to_keysym("KP_1"))
x11_linux_key_num2 = display.keysym_to_keycode(XK.string_to_keysym("KP_2"))
x11_linux_key_num3 = display.keysym_to_keycode(XK.string_to_keysym("KP_3"))
x11_linux_key_num4 = display.keysym_to_keycode(XK.string_to_keysym("KP_4"))
x11_linux_key_num5 = display.keysym_to_keycode(XK.string_to_keysym("KP_5"))
x11_linux_key_num6 = display.keysym_to_keycode(XK.string_to_keysym("KP_6"))
x11_linux_key_num7 = display.keysym_to_keycode(XK.string_to_keysym("KP_7"))
x11_linux_key_num8 = display.keysym_to_keycode(XK.string_to_keysym("KP_8"))
x11_linux_key_num9 = display.keysym_to_keycode(XK.string_to_keysym("KP_9"))
x11_linux_key_multiply = display.keysym_to_keycode(XK.string_to_keysym("KP_Multiply"))
x11_linux_key_add = display.keysym_to_keycode(XK.string_to_keysym("KP_Add"))
x11_linux_key_separator = display.keysym_to_keycode(XK.string_to_keysym("KP_Separator"))
x11_linux_key_subtract = display.keysym_to_keycode(XK.string_to_keysym("KP_Subtract"))
x11_linux_key_decimal = display.keysym_to_keycode(XK.string_to_keysym("KP_Decimal"))
x11_linux_key_divide = display.keysym_to_keycode(XK.string_to_keysym("KP_Divide"))
x11_linux_key_f1 = display.keysym_to_keycode(XK.string_to_keysym("F1"))
x11_linux_key_f2 = display.keysym_to_keycode(XK.string_to_keysym("F2"))
x11_linux_key_f3 = display.keysym_to_keycode(XK.string_to_keysym("F3"))
x11_linux_key_f4 = display.keysym_to_keycode(XK.string_to_keysym("F4"))
x11_linux_key_f5 = display.keysym_to_keycode(XK.string_to_keysym("F5"))
x11_linux_key_f6 = display.keysym_to_keycode(XK.string_to_keysym("F6"))
x11_linux_key_f7 = display.keysym_to_keycode(XK.string_to_keysym("F7"))
x11_linux_key_f8 = display.keysym_to_keycode(XK.string_to_keysym("F8"))
x11_linux_key_f9 = display.keysym_to_keycode(XK.string_to_keysym("F9"))
x11_linux_key_f10 = display.keysym_to_keycode(XK.string_to_keysym("F10"))
x11_linux_key_f11 = display.keysym_to_keycode(XK.string_to_keysym("F11"))
x11_linux_key_f12 = display.keysym_to_keycode(XK.string_to_keysym("F12"))
x11_linux_key_f13 = display.keysym_to_keycode(XK.string_to_keysym("F13"))
x11_linux_key_f14 = display.keysym_to_keycode(XK.string_to_keysym("F14"))
x11_linux_key_f15 = display.keysym_to_keycode(XK.string_to_keysym("F15"))
x11_linux_key_f16 = display.keysym_to_keycode(XK.string_to_keysym("F16"))
x11_linux_key_f17 = display.keysym_to_keycode(XK.string_to_keysym("F17"))
x11_linux_key_f18 = display.keysym_to_keycode(XK.string_to_keysym("F18"))
x11_linux_key_f19 = display.keysym_to_keycode(XK.string_to_keysym("F19"))
x11_linux_key_f20 = display.keysym_to_keycode(XK.string_to_keysym("F20"))
x11_linux_key_f21 = display.keysym_to_keycode(XK.string_to_keysym("F21"))
x11_linux_key_f22 = display.keysym_to_keycode(XK.string_to_keysym("F22"))
x11_linux_key_f23 = display.keysym_to_keycode(XK.string_to_keysym("F23"))
x11_linux_key_f24 = display.keysym_to_keycode(XK.string_to_keysym("F24"))
x11_linux_key_numlock = display.keysym_to_keycode(XK.string_to_keysym("Num_Lock"))
x11_linux_key_scrolllock = display.keysym_to_keycode(XK.string_to_keysym("Scroll_Lock"))
x11_linux_key_shiftleft = display.keysym_to_keycode(XK.string_to_keysym("Shift_L"))
x11_linux_key_shiftright = display.keysym_to_keycode(XK.string_to_keysym("Shift_R"))
x11_linux_key_ctrlleft = display.keysym_to_keycode(XK.string_to_keysym("Control_L"))
x11_linux_key_ctrlright = display.keysym_to_keycode(XK.string_to_keysym("Control_R"))
x11_linux_key_altleft = display.keysym_to_keycode(XK.string_to_keysym("Alt_L"))
x11_linux_key_altright = display.keysym_to_keycode(XK.string_to_keysym("Alt_R"))
x11_linux_key_space = display.keysym_to_keycode(XK.string_to_keysym("space"))
x11_linux_key_newline_n = display.keysym_to_keycode(XK.string_to_keysym("Return"))
x11_linux_key_newline_r = display.keysym_to_keycode(XK.string_to_keysym("Return"))
x11_linux_key_newline_t = display.keysym_to_keycode(XK.string_to_keysym("Escape"))
x11_linux_key_exclam = display.keysym_to_keycode(XK.string_to_keysym("exclam"))
x11_linux_key_numbersign = display.keysym_to_keycode(XK.string_to_keysym("numbersign"))
x11_linux_key_percent = display.keysym_to_keycode(XK.string_to_keysym("percent"))
x11_linux_key_dollar = display.keysym_to_keycode(XK.string_to_keysym("dollar"))
x11_linux_key_ampersand = display.keysym_to_keycode(XK.string_to_keysym("ampersand"))
x11_linux_key_quotedbl = display.keysym_to_keycode(XK.string_to_keysym("quotedbl"))
x11_linux_key_apostrophe = display.keysym_to_keycode(XK.string_to_keysym("apostrophe"))
x11_linux_key_parenleft = display.keysym_to_keycode(XK.string_to_keysym("parenleft"))
x11_linux_key_parenright = display.keysym_to_keycode(XK.string_to_keysym("parenright"))
x11_linux_key_asterisk = display.keysym_to_keycode(XK.string_to_keysym("asterisk"))
x11_linux_key_equal = display.keysym_to_keycode(XK.string_to_keysym("equal"))
x11_linux_key_plus = display.keysym_to_keycode(XK.string_to_keysym("plus"))
x11_linux_key_comma = display.keysym_to_keycode(XK.string_to_keysym("comma"))
x11_linux_key_minus = display.keysym_to_keycode(XK.string_to_keysym("minus"))
x11_linux_key_period = display.keysym_to_keycode(XK.string_to_keysym("period"))
x11_linux_key_slash = display.keysym_to_keycode(XK.string_to_keysym("slash"))
x11_linux_key_colon = display.keysym_to_keycode(XK.string_to_keysym("colon"))
x11_linux_key_semicolon = display.keysym_to_keycode(XK.string_to_keysym("semicolon"))
x11_linux_key_less = display.keysym_to_keycode(XK.string_to_keysym("less"))
x11_linux_key_greater = display.keysym_to_keycode(XK.string_to_keysym("greater"))
x11_linux_key_question = display.keysym_to_keycode(XK.string_to_keysym("question"))
x11_linux_key_at = display.keysym_to_keycode(XK.string_to_keysym("at"))
x11_linux_key_bracketleft = display.keysym_to_keycode(XK.string_to_keysym("bracketleft"))
x11_linux_key_bracketright = display.keysym_to_keycode(XK.string_to_keysym("bracketright"))
x11_linux_key_backslash = display.keysym_to_keycode(XK.string_to_keysym("backslash"))
x11_linux_key_asciicircum = display.keysym_to_keycode(XK.string_to_keysym("asciicircum"))
x11_linux_key_underscore = display.keysym_to_keycode(XK.string_to_keysym("underscore"))
x11_linux_key_grave = display.keysym_to_keycode(XK.string_to_keysym("grave"))
x11_linux_key_braceleft = display.keysym_to_keycode(XK.string_to_keysym("braceleft"))
x11_linux_key_bar = display.keysym_to_keycode(XK.string_to_keysym("bar"))
x11_linux_key_braceright = display.keysym_to_keycode(XK.string_to_keysym("braceright"))
x11_linux_key_asciitilde = display.keysym_to_keycode(XK.string_to_keysym("asciitilde"))
x11_linux_key_a = display.keysym_to_keycode(XK.string_to_keysym("a"))
x11_linux_key_b = display.keysym_to_keycode(XK.string_to_keysym("b"))
x11_linux_key_c = display.keysym_to_keycode(XK.string_to_keysym("c"))
x11_linux_key_d = display.keysym_to_keycode(XK.string_to_keysym("d"))
x11_linux_key_e = display.keysym_to_keycode(XK.string_to_keysym("e"))
x11_linux_key_f = display.keysym_to_keycode(XK.string_to_keysym("f"))
x11_linux_key_g = display.keysym_to_keycode(XK.string_to_keysym("g"))
x11_linux_key_h = display.keysym_to_keycode(XK.string_to_keysym("h"))
x11_linux_key_i = display.keysym_to_keycode(XK.string_to_keysym("i"))
x11_linux_key_j = display.keysym_to_keycode(XK.string_to_keysym("j"))
x11_linux_key_k = display.keysym_to_keycode(XK.string_to_keysym("k"))
x11_linux_key_l = display.keysym_to_keycode(XK.string_to_keysym("l"))
x11_linux_key_m = display.keysym_to_keycode(XK.string_to_keysym("m"))
x11_linux_key_n = display.keysym_to_keycode(XK.string_to_keysym("n"))
x11_linux_key_o = display.keysym_to_keycode(XK.string_to_keysym("o"))
x11_linux_key_p = display.keysym_to_keycode(XK.string_to_keysym("p"))
x11_linux_key_q = display.keysym_to_keycode(XK.string_to_keysym("q"))
x11_linux_key_r = display.keysym_to_keycode(XK.string_to_keysym("r"))
x11_linux_key_s = display.keysym_to_keycode(XK.string_to_keysym("s"))
x11_linux_key_t = display.keysym_to_keycode(XK.string_to_keysym("t"))
x11_linux_key_u = display.keysym_to_keycode(XK.string_to_keysym("u"))
x11_linux_key_v = display.keysym_to_keycode(XK.string_to_keysym("v"))
x11_linux_key_w = display.keysym_to_keycode(XK.string_to_keysym("w"))
x11_linux_key_x = display.keysym_to_keycode(XK.string_to_keysym("x"))
x11_linux_key_y = display.keysym_to_keycode(XK.string_to_keysym("y"))
x11_linux_key_z = display.keysym_to_keycode(XK.string_to_keysym("z"))
x11_linux_key_A = display.keysym_to_keycode(XK.string_to_keysym("A"))
x11_linux_key_B = display.keysym_to_keycode(XK.string_to_keysym("B"))
x11_linux_key_C = display.keysym_to_keycode(XK.string_to_keysym("C"))
x11_linux_key_D = display.keysym_to_keycode(XK.string_to_keysym("D"))
x11_linux_key_E = display.keysym_to_keycode(XK.string_to_keysym("E"))
x11_linux_key_F = display.keysym_to_keycode(XK.string_to_keysym("F"))
x11_linux_key_G = display.keysym_to_keycode(XK.string_to_keysym("G"))
x11_linux_key_H = display.keysym_to_keycode(XK.string_to_keysym("H"))
x11_linux_key_I = display.keysym_to_keycode(XK.string_to_keysym("I"))
x11_linux_key_J = display.keysym_to_keycode(XK.string_to_keysym("J"))
x11_linux_key_K = display.keysym_to_keycode(XK.string_to_keysym("K"))
x11_linux_key_L = display.keysym_to_keycode(XK.string_to_keysym("L"))
x11_linux_key_M = display.keysym_to_keycode(XK.string_to_keysym("M"))
x11_linux_key_N = display.keysym_to_keycode(XK.string_to_keysym("N"))
x11_linux_key_O = display.keysym_to_keycode(XK.string_to_keysym("O"))
x11_linux_key_P = display.keysym_to_keycode(XK.string_to_keysym("P"))
x11_linux_key_Q = display.keysym_to_keycode(XK.string_to_keysym("Q"))
x11_linux_key_R = display.keysym_to_keycode(XK.string_to_keysym("R"))
x11_linux_key_S = display.keysym_to_keycode(XK.string_to_keysym("s"))
x11_linux_key_T = display.keysym_to_keycode(XK.string_to_keysym("T"))
x11_linux_key_U = display.keysym_to_keycode(XK.string_to_keysym("U"))
x11_linux_key_V = display.keysym_to_keycode(XK.string_to_keysym("V"))
x11_linux_key_W = display.keysym_to_keycode(XK.string_to_keysym("W"))
x11_linux_key_X = display.keysym_to_keycode(XK.string_to_keysym("X"))
x11_linux_key_Y = display.keysym_to_keycode(XK.string_to_keysym("Y"))
x11_linux_key_Z = display.keysym_to_keycode(XK.string_to_keysym("Z"))
x11_linux_key_1 = display.keysym_to_keycode(XK.string_to_keysym("1"))
x11_linux_key_2 = display.keysym_to_keycode(XK.string_to_keysym("2"))
x11_linux_key_3 = display.keysym_to_keycode(XK.string_to_keysym("3"))
x11_linux_key_4 = display.keysym_to_keycode(XK.string_to_keysym("4"))
x11_linux_key_5 = display.keysym_to_keycode(XK.string_to_keysym("5"))
x11_linux_key_6 = display.keysym_to_keycode(XK.string_to_keysym("6"))
x11_linux_key_7 = display.keysym_to_keycode(XK.string_to_keysym("7"))
x11_linux_key_8 = display.keysym_to_keycode(XK.string_to_keysym("8"))
x11_linux_key_9 = display.keysym_to_keycode(XK.string_to_keysym("9"))
x11_linux_key_0 = display.keysym_to_keycode(XK.string_to_keysym("0"))

x11_linux_mouse_left = 1
x11_linux_mouse_middle = 2
x11_linux_mouse_right = 3
x11_linux_scroll_direction_up = 4
x11_linux_scroll_direction_down = 5
x11_linux_scroll_direction_left = 6
x11_linux_scroll_direction_right = 7

keyboard_keys_table = {
    "backspace": x11_linux_key_backspace,
    "\b": x11_linux_key_slash_b,
    "tab": x11_linux_key_tab,
    "enter": x11_linux_key_enter,
    "return": x11_linux_key_return,
    "shift": x11_linux_key_shift,
    "ctrl": x11_linux_key_ctrl,
    "alt": x11_linux_key_alt,
    "pause": x11_linux_key_pause,
    "capslock": x11_linux_key_capslock,
    "esc": x11_linux_key_esc,
    "pgup": x11_linux_key_pgup,
    "pgdn": x11_linux_key_pgdn,
    "pageup": x11_linux_key_pageup,
    "pagedown": x11_linux_key_pagedown,
    "end": x11_linux_key_end,
    "home": x11_linux_key_home,
    "left": x11_linux_key_left,
    "up": x11_linux_key_up,
    "right": x11_linux_key_right,
    "down": x11_linux_key_down,
    "select": x11_linux_key_select,
    "print": x11_linux_key_print,
    "execute": x11_linux_key_execute,
    "prtsc": x11_linux_key_prtsc,
    "prtscr": x11_linux_key_prtscr,
    "prntscrn": x11_linux_key_prntscrn,
    "insert": x11_linux_key_insert,
    "del": x11_linux_key_del,
    "delete": x11_linux_key_delete,
    "help": x11_linux_key_help,
    "win": x11_linux_key_win,
    "winleft": x11_linux_key_winleft,
    "winright": x11_linux_key_winright,
    "apps": x11_linux_key_apps,
    "num0": x11_linux_key_num0,
    "num1": x11_linux_key_num1,
    "num2": x11_linux_key_num2,
    "num3": x11_linux_key_num3,
    "num4": x11_linux_key_num4,
    "num5": x11_linux_key_num5,
    "num6": x11_linux_key_num6,
    "num7": x11_linux_key_num7,
    "num8": x11_linux_key_num8,
    "num9": x11_linux_key_num9,
    "multiply": x11_linux_key_multiply,
    "add": x11_linux_key_add,
    "separator": x11_linux_key_separator,
    "subtract": x11_linux_key_subtract,
    "decimal": x11_linux_key_decimal,
    "divide": x11_linux_key_divide,
    "f1": x11_linux_key_f1,
    "f2": x11_linux_key_f2,
    "f3": x11_linux_key_f3,
    "f4": x11_linux_key_f4,
    "f5": x11_linux_key_f5,
    "f6": x11_linux_key_f6,
    "f7": x11_linux_key_f7,
    "f8": x11_linux_key_f8,
    "f9": x11_linux_key_f9,
    "f10": x11_linux_key_f10,
    "f11": x11_linux_key_f11,
    "f12": x11_linux_key_f12,
    "f13": x11_linux_key_f13,
    "f14": x11_linux_key_f14,
    "f15": x11_linux_key_f15,
    "f16": x11_linux_key_f16,
    "f17": x11_linux_key_f17,
    "f18": x11_linux_key_f18,
    "f19": x11_linux_key_f19,
    "f20": x11_linux_key_f20,
    "f21": x11_linux_key_f21,
    "f22": x11_linux_key_f22,
    "f23": x11_linux_key_f23,
    "f24": x11_linux_key_f24,
    "numlock": x11_linux_key_numlock,
    "scrolllock": x11_linux_key_scrolllock,
    "shiftleft": x11_linux_key_shiftleft,
    "shiftright": x11_linux_key_shiftright,
    "ctrlleft": x11_linux_key_ctrlleft,
    "ctrlright": x11_linux_key_ctrlright,
    "altleft": x11_linux_key_altleft,
    "altright": x11_linux_key_altright,
    "space": x11_linux_key_space,
    "\n": x11_linux_key_newline_n,
    "\r": x11_linux_key_newline_r,
    "\t": x11_linux_key_newline_t,
    "!": x11_linux_key_exclam,
    "#": x11_linux_key_numbersign,
    "%": x11_linux_key_percent,
    "$": x11_linux_key_dollar,
    "&": x11_linux_key_ampersand,
    '"': x11_linux_key_quotedbl,
    "'": x11_linux_key_apostrophe,
    "(": x11_linux_key_parenleft,
    ")": x11_linux_key_parenright,
    "*": x11_linux_key_asterisk,
    "=": x11_linux_key_equal,
    "+": x11_linux_key_plus,
    ",": x11_linux_key_comma,
    "-": x11_linux_key_minus,
    ".": x11_linux_key_period,
    "/": x11_linux_key_slash,
    ":": x11_linux_key_colon,
    ";": x11_linux_key_semicolon,
    "<": x11_linux_key_less,
    ">": x11_linux_key_greater,
    "?": x11_linux_key_question,
    "@": x11_linux_key_at,
    "[": x11_linux_key_bracketleft,
    "]": x11_linux_key_bracketright,
    "\\": x11_linux_key_backslash,
    "^": x11_linux_key_asciicircum,
    "_": x11_linux_key_underscore,
    "`": x11_linux_key_grave,
    "{": x11_linux_key_braceleft,
    "|": x11_linux_key_bar,
    "}": x11_linux_key_braceright,
    "~": x11_linux_key_asciitilde,
    "a": x11_linux_key_a,
    "b": x11_linux_key_b,
    "c": x11_linux_key_c,
    "d": x11_linux_key_d,
    "e": x11_linux_key_e,
    "f": x11_linux_key_f,
    "g": x11_linux_key_g,
    "h": x11_linux_key_h,
    "i": x11_linux_key_i,
    "j": x11_linux_key_j,
    "k": x11_linux_key_k,
    "l": x11_linux_key_l,
    "m": x11_linux_key_m,
    "n": x11_linux_key_n,
    "o": x11_linux_key_o,
    "p": x11_linux_key_p,
    "q": x11_linux_key_q,
    "r": x11_linux_key_r,
    "s": x11_linux_key_s,
    "t": x11_linux_key_t,
    "u": x11_linux_key_u,
    "v": x11_linux_key_v,
    "w": x11_linux_key_w,
    "x": x11_linux_key_x,
    "y": x11_linux_key_y,
    "z": x11_linux_key_z,
    "A": x11_linux_key_A,
    "B": x11_linux_key_B,
    "C": x11_linux_key_C,
    "D": x11_linux_key_D,
    "E": x11_linux_key_E,
    "F": x11_linux_key_F,
    "G": x11_linux_key_G,
    "H": x11_linux_key_H,
    "I": x11_linux_key_I,
    "J": x11_linux_key_J,
    "K": x11_linux_key_K,
    "L": x11_linux_key_L,
    "M": x11_linux_key_M,
    "N": x11_linux_key_N,
    "O": x11_linux_key_O,
    "P": x11_linux_key_P,
    "Q": x11_linux_key_Q,
    "R": x11_linux_key_R,
    "S": x11_linux_key_S,
    "T": x11_linux_key_T,
    "U": x11_linux_key_U,
    "V": x11_linux_key_V,
    "W": x11_linux_key_W,
    "X": x11_linux_key_X,
    "Y": x11_linux_key_Y,
    "Z": x11_linux_key_Z,
    "1": x11_linux_key_1,
    "2": x11_linux_key_2,
    "3": x11_linux_key_3,
    "4": x11_linux_key_4,
    "5": x11_linux_key_5,
    "6": x11_linux_key_6,
    "7": x11_linux_key_7,
    "8": x11_linux_key_8,
    "9": x11_linux_key_9,
    "0": x11_linux_key_0,
}
mouse_keys_table = {
    "mouse_left": x11_linux_mouse_left,
    "mouse_middle": x11_linux_mouse_middle,
    "mouse_right": x11_linux_mouse_right
}
special_mouse_keys_table = {
    "scroll_up": x11_linux_scroll_direction_up,
    "scroll_down": x11_linux_scroll_direction_down,
    "scroll_left": x11_linux_scroll_direction_left,
    "scroll_right": x11_linux_scroll_direction_right
}
keyboard = x11_linux_keyboard_control
keyboard_check = x11_linux_listener
mouse = x11_linux_mouse_control
screen = x11_linux_screen
recorder = x11_linux_recoder


class KeypressHandler(Thread):

    def __init__(self, default_daemon: bool = True):
        """
        default damon is true
        still listener : continue listener keycode ?
        event_key_code : now current key code default is 0
        """
        super().__init__()
        self.daemon = default_daemon
        self.still_listener = True
        self.record_flag = False
        self.record_queue = None
        self.event_keycode = 0
        self.event_position = 0, 0

    # two times because press and release
    def check_is_press(self, keycode: int) -> bool:
        """
        :param keycode we want to check
        """
        if keycode == self.event_keycode:
            self.event_keycode = 0
            return True
        else:
            return False

    def run(self, reply) -> None:
        """
        :param reply listener return data
        get data
        while data not null and still listener
            get event

        """
        try:
            data = reply.data
            while len(data) and self.still_listener:
                event, data = rq.EventField(None).parse_binary_value(data, current_display.display, None, None)
                if event.detail != 0:
                    if event.type is X.ButtonRelease or event.type is X.KeyRelease:
                        self.event_keycode = event.detail
                        self.event_position = event.root_x, event.root_y
                        if self.record_flag is True:
                            temp = (event.type, event.detail, event.root_x, event.root_y)
                            self.record_queue.put(temp)
        except:
            e = sys.exc_info()[0]
            raise e
       # except AutoControlException:
            #raise AutoControlException(listener_error)

    def record(self, record_queue) -> None:
        """
        :param record_queue the queue test_record action
        """
        self.record_flag = True
        self.record_queue = record_queue

    def stop_record(self) -> Queue:
        self.record_flag = False
        return self.record_queue


class XWindowsKeypressListener(Thread):

    def __init__(self, default_daemon=True):
        """
        :param default_daemon default kill when program down
        create handler
        set root
        """
        super().__init__()
        self.daemon = default_daemon
        self.still_listener = True
        self.handler = KeypressHandler()
        self.root = current_display.screen().root
        self.context = None

    def check_is_press(self, keycode: int):
        """
        :param keycode check this keycode is press?
        """
        return self.handler.check_is_press(keycode)

    def run(self) -> None:
        """
        while still listener
            get context
            set handler
            set test_record
            get event
        """
        if self.still_listener:
            try:
                # Monitor keypress and button press
                if self.context is None:
                    self.context = current_display.record_create_context(
                        0,
                        [record.AllClients],
                        [{
                            'core_requests': (0, 0),
                            'core_replies': (0, 0),
                            'ext_requests': (0, 0, 0, 0),
                            'ext_replies': (0, 0, 0, 0),
                            'delivered_events': (0, 0),
                            'device_events': (X.KeyReleaseMask, X.ButtonReleaseMask),
                            'errors': (0, 0),
                            'client_started': False,
                            'client_died': False,
                        }])
                    current_display.record_enable_context(self.context, self.handler.run)
                    current_display.record_free_context(self.context)
                # keep running this to get event
                self.root.display.next_event()
            except AutoControlException:
                raise AutoControlException(listener_error)
            finally:
                self.handler.still_listener = False
                self.still_listener = False

    def record(self, record_queue) -> None:
        self.handler.record(record_queue)

    def stop_record(self) -> Queue:
        return self.handler.stop_record()


xwindows_listener = XWindowsKeypressListener()
xwindows_listener.start()


def check_key_is_press(keycode: int) -> int:
    """
    :param keycode check this keycode is press?
    """
    return xwindows_listener.check_is_press(keycode)


def x11_linux_record(record_queue) -> None:
    """
    :param record_queue the queue test_record action
    """
    xwindows_listener.record(record_queue)


def x11_linux_stop_record() -> Queue:
    """
    stop test_record action
    """
    return xwindows_listener.stop_record()
