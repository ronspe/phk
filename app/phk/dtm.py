# -*- coding: utf-8 -*-
import os
import sys
app_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", ".."))
sys.path.append(app_path)
# print('\n'.join(sys.path))

import re
import pickle
import copy
from typing import Generator
import app
from app.config.ini import Ini, fallback_dtm
from app.phk.dispatch import Dispatch
import PySimpleGUI as sg
import app.config as config
log = config.default_logger
# themes: https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion


class DtmMenu:

    search_string = ''

    def __init__(self):
        self.menu = []  # The list in the menu that can be searched
        self.hotkeys = []
        self.phrases = []
        self.load_pickled()
        self._set_dtm_look_and_feel()
        self._get_phrases_and_hotkeys_menu()
        sg.theme(self.dtm_theme)
        layout = [[sg.Text(f"Fuzzy search {len(self.menu)} descriptions:")],
                  [sg.InputText(default_text='', key='-searchstring-', focus=True)],
                  [sg.Listbox(values=self._get_filtered_menu_items(),
                              size=(self.dtm_width, self.dtm_height),
                              font=('Calibri', 12),
                              key='_menuitems_')
                  ],
                 ]

        self.window = sg.Window('PHK menu',
                                layout,
                                return_keyboard_events=True,
                                location=(self.dtm_pos_x, self.dtm_pos_y),
                                use_default_focus=False,
                                no_titlebar=False,
                                keep_on_top=False
                               )

        quit_keys = ["Escape:27", chr(27)]
        enter_keys = ["Return:13", chr(13)]
        while True:
            event, values = self.window.read()
            if event in quit_keys:
                break
            elif event in enter_keys:
                selected = self._get_filtered_menu_items()[0]
                self._callback(selected)
                break
            else:
                value = self.window.FindElement('-searchstring-').Get()
                self.search_string = value
                self._update_list()
        self.window.close()



    def _set_dtm_look_and_feel(self) -> None:
        """
        Set the Theme, Width, Height and position of the menu.
        width: Width of the window.
        height: Height of the window.
        x: Position from the left part of the screen.
        y: Position from the top part of the screen.
        theme: An available named theme from pySimpleGUI, see all available:
               https://pysimplegui.readthedocs.io/en/latest/cookbook/#look-and-feel-theme-explosion
        """
        ini = Ini()
        self.dtm_width = ini.read_int_value("dtm_width", fallback=fallback_dtm.width)
        self.dtm_height = ini.read_int_value("dtm_height", fallback=fallback_dtm.height)
        self.dtm_pos_x = ini.read_int_value("dtm_pos_x", fallback=fallback_dtm.pos_x)
        self.dtm_pos_y = ini.read_int_value("dtm_pos_y", fallback=fallback_dtm.pos_y)
        self.dtm_theme = ini.read_string_value("dtm_theme", fallback=fallback_dtm.theme)

    def _get_filtered_menu_items(self) -> list:
        """
        list by a fuzzy filter.
        """
        items = []
        menu = self._filtermenu()
        for item in menu:
            items.append(item)
        return items

    def _update_list(self) -> None:
        """
        Update the menu list by a fuzzy filter.
        """
        items = self._get_filtered_menu_items()
        self.window.FindElement('_menuitems_').Update(items)

    def _callback(self, selected) -> None:
        """
        Is called when an item is selected by mouse or Return.
        It will search the phrases and hotkeys lists based on the
        trigger (They must be unique) and then dispatch the action.
        """
        match = re.search("(^.*\[)(.*)(\].*$)", selected)
        trigger = match.group(2)
        all_commands = self.phrases + self.hotkeys
        for command in all_commands:
            if trigger in command:
                Dispatch().actions(command)

    def _get_phrases_and_hotkeys_menu(self) -> None:
        """
        Makes a temporary list for the menu by
        copying the hotkeys and the phrases, adding a numbering
        and sorting them.
        """
        ph = copy.deepcopy(self.phrases)
        hk = copy.deepcopy(self.hotkeys)
        self.menu = ph + hk
        for item in self.menu:
            search_string = f"{item[4]} [{item[1]}]"
            item.insert(0, search_string)
        self.menu.sort()
        for i, item in enumerate(self.menu):
            item[0] = "({:02}): {}".format(i, item[0])

    def _filtermenu(self) -> Generator:
        """
        Filter self.menu with fuzzy search
        :return: A filtered and sorted list.
        """
        results = []
        pattern1 = ".*?".join(map(re.escape, self.search_string))
        pattern2 = f"(?=({pattern1}))"
        regex = re.compile(pattern2, re.IGNORECASE)
        l = lambda x: x
        for item in self.menu:
            rl = list(regex.finditer(l(item[5])))
            if rl:
                sm = min(rl, key=lambda x: len(x.group(1)))
                results.append((len(sm.group(1)), sm.start(), l(item[5]), item[0]))
        return (z[-1] for z in sorted(results))

    def load_pickled(self) -> None:
        """
        Load the pickled hotkeys and phrases from file.
        """
        hotkey_pickle = app.dtm.hotkeys
        phrase_pickle = app.dtm.phrases
        try:
            with open(hotkey_pickle, "rb") as f:
                log.debug(f"Load pickle: {hotkey_pickle}")
                self.hotkeys = pickle.load(f)
        except FileNotFoundError:
            log.error(f"Could not load pickle: {hotkey_pickle}", exc_info=True)
        try:
            with open(phrase_pickle, "rb") as f:
                log.debug(f"Load pickle: {phrase_pickle}")
                self.phrases = pickle.load(f)
        except FileNotFoundError:
            log.error(f"Could not load pickle: {phrase_pickle}", exc_info=True)

        
if __name__ == "__main__":
    DtmMenu()
