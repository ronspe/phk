# -*- coding: utf-8 -*-
import re
from pynput.keyboard import Key, KeyCode

from app.config.button import Button
import app.config as config
log = config.default_logger

# Expression to split text from possible special keys
KEY_SPLITTER = re.compile("(<[^<>]+>\+?)")


class KeyConvert:
    """
    Converting keycodes to representation of keys.
    Key = The Key as defined in pyinput.keyboard.Key e.g.: Key.enter
    Representation = A string used by the user to name a Key. e.g.: <enter>
    """
    def __init__(self):
        self.button = Button()

    # TODO move these functions?
    def _is_key_representation(self, key_representation: str) -> bool:
        """
        Checks if a string represents a special key.
        :param key_representation: A string representing a key. e.g.: <tab>
        :return: True if the string represents a defined keycode
        """
        if key_representation in dir(self.button):
            return True
        else:
            return False

    @staticmethod
    def _clean_key_representation(key_representation: str) -> str:
        """
        Cleans up the string of a key_representation.
        Removes spaces and bracket around the text and make it upper-case
        :param key_representation: a string representing a key. e.g.: <tab>
        """
        clean_string = key_representation.upper().strip()
        clean_string = clean_string.lstrip("<")
        clean_string = clean_string.rstrip(">")
        # in case there is still a space left from within the brackets
        clean_string = (clean_string.strip())
        return clean_string

    def get_key_code(self, key_representation: str) -> bool:
        """
        Returns the key based on a string that represents a special key.
        :param key_representation: a string representing a key. e.g.: <tab>
        """
        clean_key_code = self._clean_key_representation(key_representation)
        if self._is_key_representation(clean_key_code):
            return getattr(self.button, clean_key_code)
        else:
            return False

    def get_key_representation(self, key: Key) -> object:
        """
        Converts a KeyCode to a string representing a key.
        If the Key is just a character, return the character.
        :param key: A keyCode. e.g.: Key.enter
        Example:
            Key.enter -> <enter>
            Key.tab -> <tab>
            a -> "a"
            etc.
        """
        key_code = str(KeyCode.from_char(key))
        key_representation = key_code.replace("Key.", "")
        key_representation = key_representation.replace('"', '').replace("'", "")
        if len(key_representation) != 1:
            key_representation = "<{}>".format(key_representation)
        if key_representation is None:
            log.warning("No key representation found for:{}".format(key))
        return key_representation

    def get_hotkey_keys(self, hotkey: str) -> list:
        """
        Returns the keys as a set based on a key representation.
        e.g.: "<ctrl>m" -> [<Key.ctrl: <65507>>, 'm']
        Used when adding hotkeys.
        :param hotkey: A string representing a complete hotkey.
               e.g.:"<alt><esc><f3>"
        :return: A list containing all keys in the hotkey representation
        """
        hotkey_buttons = []
        for part in KEY_SPLITTER.split(hotkey):
            if part != "":
                special_key = self.get_key_code(part)
                if special_key:
                    hotkey_buttons.append(self.get_key_code(part))
                else:
                    hotkey_buttons.append(KeyCode(char=part))
        #print(hotkey_buttons)
        return hotkey_buttons

    @staticmethod
    def is_special_key(key_representation: str) -> bool:
        try:
            if len(key_representation) == 1 :
                return False
            else:
                return True
        except TypeError:
            log.error("Key not recognized:[{}]".format(key_representation))

    @staticmethod
    def is_single_character(key_representation: str) -> bool:
        try:
            if len(key_representation) == 1 :
                return True
            else:
                return False
        except TypeError:
            log.error("Key not recognized:[{}]".format(key_representation))

    def lowercase_char(self, key: KeyCode) -> KeyCode:
        """
        Make a char lowercase, needed when the hotkey contains
        a <shift> because it will register an uppercase.
        Special keys are not changed.
        :param key: A KeyCode. e.g.: Key.enter
        """
        key_representation = self.get_key_representation(key)
        # Only chars will be of the lenght 1, specials keys are larger.
        try:
            if self.is_single_character(key_representation):
                # Make it lowercase and convert it back to a KeyCode.
                key = KeyCode(char=key_representation.lower())
        except TypeError:
            log.error("Key not recognized:[{}]".format(key))
        return key

    def count_characters(self, hotkey: str) -> int:
        """
        Returns the amount of single characters that are in a hotkey representation.
        If a key representation is a special key, it will be ignored.
        This is used to determine how many times we should backspace a typed character.
        :param hotkey: A string representing a complete hotkey. e.g.:"<alt><esc>a"
        :return: The number of typed single characters e.g.: 1
        """
        character_count = 0
        for part in KEY_SPLITTER.split(hotkey):
            if part != "":
                special_key = self.get_key_code(part)
                # hack to make sure a space, tab or enter is also removed in the
                # trigger of a phrase. e.g.: <space>btw<space>
                # TODO: fix this correctly...
                if (part == "<space>") or (part == "<tab>") or (part == "<enter>"):
                    special_key = False
                    part = " "
                if not special_key:
                    character_count = character_count + len(part)
        return character_count










