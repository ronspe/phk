# -*- coding: utf-8 -*-
"""
Contains all actions that can be trigerred by a hotkey or prase.
"""
import time
import subprocess
import app
import app.config as config
from pynput.keyboard import Controller, Key
from pyautogui import typewrite
from app.phk.key_convert import KeyConvert, KEY_SPLITTER

log = config.default_logger


def type_keys(text: str, raw: bool = False, interval: float = 0.05) -> None:
    """
    Type a text to the screen including special keys.
    as defined in config/button.py
    :param text: The text to type.
    :param raw: If True the text is typed without converting the codes for special keys.
                e.g.:
                raw=False : "a<tab>b" --> "a     b"
                raw=True  : "a<tab>b" --> "a<tab>b"
                Default is False.
    :param interval: The delay in time between typing characters. Default = 0.05
    """
    #print(text)
    keyboard = Controller()
    key_convert = KeyConvert()
    if not raw:
        for part in KEY_SPLITTER.split(text):
            time.sleep(interval)
            special_key = key_convert.get_key_code(part)
            if special_key:
                keyboard.press(special_key)
                keyboard.release(special_key)
            else:
                # temporary hack because of bug in typewriter
                # the "<" is returned as a ">"
                # for now we type: "<shift>,"
                if "<" in part:
                    for i in part:
                        if i == "<":
                            keyboard.press(Key.shift)
                            keyboard.press(',')
                            keyboard.release(',')
                            keyboard.release(Key.shift)
                        else:
                            keyboard.press(i)
                            keyboard.release(i)
                else:
                    typewrite(part, interval=interval)
    else:
        # TODO: typewrite has a problem with "<" it comes out like ">"
        typewrite(text, interval=interval)


def return_shell_command_output(cmd: str) -> str:
    """
    Execute a shell command and return the output.
    :param cmd: The command to execute.
    :return: The output of the command as a string.
    """
    log.debug(f"cmd:[{cmd}]")
    with subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, bufsize=-1, universal_newlines=True
    ) as proc:
        result = proc.communicate()[0][:-1]
        if proc.returncode:  # in case the exit-code in not zero
            raise subprocess.CalledProcessError(proc.returncode, result)
        return result


def run_shell_command(cmd: str) -> None:
    """
    Execute a shell command.
    :param cmd: The command to execute.
    https://pymotw.com/2/subprocess/
    https://www.programcreek.com/python/example/50/subprocess.Popen
    """
    log.debug(f"cmd:[{cmd}]")
    subprocess.Popen(str(cmd), shell=True, bufsize=-1)


def run_python(file_name):
    if type(file_name) == tuple:
        file_name = file_name[0]
    cmd = f"{app.system.venv_python} {file_name}"
    log.debug(f"cmd:[{cmd}]")
    subprocess.Popen(cmd, shell=True, bufsize=-1)
