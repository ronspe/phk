# -*- coding: utf-8 -*-
import os
import importlib.util
import sys
import inspect
import app
import app.config as config
from app.config.ini import Ini, fallback_no_default_folders
from app.phk.hotkey import Hotkey
from app.phk.phrase import Phrase
import scripts.default.macro_recorder
log = config.default_logger


class LoadScripts:
    def __init__(self, kl, root_path: str, folder: str) -> None:
        """
        Load the user scripts containing the hotkeys and phrases.
        :param kl: The keyboard listener.
        :param root_path: The path to the root folder where all scripts are stored.
        :param folder: The name of the folder to read.
        """
        self.kl = kl
        self.hk = Hotkey(self.kl)
        self.ph = Phrase(self.kl)
        processed_folders = []
        # First read the 'default' folder, but only when this
        # is allowed in the config.ini.
        ini = Ini()
        # Check the config.ini if the default folder should be loaded or not
        no_default_folders = ini.read_list_values("no_default_folders",
                                                  fallback=fallback_no_default_folders)
        if folder not in no_default_folders:
            folder_path = os.path.join(root_path, app.user.default_folder)
            self._read_folder(folder_path)
            processed_folders.append(app.user.default_folder)
        # Now read the requested folder if the requested folder is not the default
        # folder otherwise we would load it twice. This can happen when no parameter
        # is given to PHK and then it defaults to 'default'.
        if folder != app.user.default_folder:
            folder_path = os.path.join(root_path, folder)
            self._read_folder(folder_path)
            processed_folders.append(folder)
        log.debug("Folders processed: {}".format(processed_folders))
        print("Phrases and hotkeys from: {}".format(processed_folders))

    def _read_folder(self, folder_path: str) -> None:
        """
        Read the directory where the user scripts are stored and
        for each file load the user script.
        :param folder_path: The full path to the folder.
        """
        try:
            files = self._filter_sort_scripts(folder_path)
            for file_name in files:
                self._load_user_script(file_name)
        except FileNotFoundError:
            sys.exit("ERROR: Folder not found:{}".format(folder_path))

    @staticmethod
    def _filter_sort_scripts(folder_path: str) -> list:
        """
        Read a folder where user configuration is stored.
        Files will be returned in alphabetic order.
        -- Return only python files
        -- Files starting with "_" will be ignored.
        :param folder_path: The full path to the folder.
        :return: A list of file names.
        """
        files = []
        for file_name in os.listdir(folder_path):
            base_name, extension = os.path.splitext(file_name)
            if extension == app.user.file_extension:
                # Ignore filenames starting with an underscore
                if not file_name[:1] == "_":
                    file_name = os.path.join(folder_path, file_name)
                    files.append(file_name)
                else:
                    log.debug(
                        "Ignored, file starts with underscore: {}".format(file_name)
                    )
            else:
                log.debug("Ignored,file is not a python file: {}".format(file_name))
        files.sort()
        return files

    def _load_user_script(self, file_name: str) -> None:
        """
        Inspect the user script and pass the module on to find the
        hotkeys[] and phrases[] lists.
        :param file_name: The name of the user script.
        """
        log.debug("Loading file: {}".format(file_name))
        spec = importlib.util.spec_from_file_location("", file_name)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        self._register_hotkeys(module, file_name)
        self._register_phrases(module, file_name)
        self._register_phrase_functions(module, file_name)

    def _register_hotkeys(self, module, file_name: str) -> None:
        """
        TODO: validate the items in the list, length, actiontype
        With the module locate the hotkeys[] list.
        Check if required fields are filled in and pass it to
        the hotkey registration where the hotkey will be checked further
        and it will be bound to the keyboard listener.
        :param module: The inspected file object
        :param file_name: The filename for reporting purposes
        """
        try:
            hotkeys = module.hotkeys
            for item in hotkeys:
                try:  # required items
                    action = item[0]
                    hotkey = item[1]
                    executable = item[2]
                except IndexError:
                    log.warning(
                        "Hotkey is missing required fields: {}, {}".format(
                            item, file_name
                        )
                    )
                    print(
                        "WARNING: Missing fields for hotkey: {} in {}".format(
                            item, os.path.basename(file_name)
                        )
                    )
                    continue
                try:  # optional items
                    description = item[3]
                except IndexError:
                    description = ""
                self.hk.register(action, hotkey, executable, description)
        except AttributeError:
            log.debug("This file has no hotkeys: {}".format(file_name))

    def _register_phrases(self, module, file_name: str) -> None:
        """
        TODO: refactor, this is getting messy
        With the module locate the phrases[] list.
        Check if required fields are filled in and pass it to
        the phrase registration where the phrase will be checked further
        and it will be bound to the keyboard listener.
        :param module: The inspected file object
        :param file_name: The filename for reporting purposes
        """
        try:
            for item in module.phrases:
                try:  # required items
                    action = item[0]
                    phrase = item[1]
                    # Minimum length of a phrase is 1 character
                    if len(phrase) == 0:
                        log.warning("Phrase is empty:{}, {}".format(item, file_name))
                        print(
                            "WARNING: Phrase is empty:{} in: {}".format(
                                item, os.path.basename(file_name)
                            )
                        )
                        continue
                    # A replacement can be any length, even zero characters.
                    replacement = item[2]
                except IndexError:
                    log.warning(
                        "Phrase is missing required fields: {}, {}".format(
                            item, file_name
                        )
                    )
                    print(
                        "WARNING: Missing fields for phrase: {} in {}".format(
                            item, os.path.basename(file_name)
                        )
                    )
                    continue
                try:  # optional items
                    description = item[3]
                except IndexError:
                    description = ""
                self.ph.register(action, phrase, replacement, description)
        except AttributeError:
            log.debug("This file has no phrases: {}".format(file_name))

    def _register_phrase_functions(self, module, file_name: str) -> None:
        """
        :param module: The inspected file object
        :param file_name: The filename for reporting purposes
        """
        import_path = f"{app.user.root_folder}."
        # FIXME: This only works on Linux and Mac, make it work on Win too.
        import_path += os.path.dirname(file_name).split('/')[-1]
        import_path += f".{os.path.basename(file_name).split('.')[0]}"
        lines = inspect.getsourcelines(module)
        for i, line in enumerate(lines[0]):
            if line.startswith("@phrasefunction"):
                next_line = lines[0][i + 1]
                phrasefunction = next_line.split('def')[1].split('(')[0].strip()
                self.ph.register_functions(import_path, phrasefunction)
        #print(phrasefunctions)
