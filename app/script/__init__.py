# -*- coding: utf-8 -*-
import os
import app
import functools

Run = app.action.run
RunPython = app.action.python
Type = app.action.type
Expand = app.action.expand
ReturnOutput = app.action.return_output


def this_folder(file):
    return os.path.dirname(os.path.realpath(file))


def this_file(file):
    return os.path.realpath(file)


def phrasefunction(prio=100):
    """
    Decorator for phrase functions.
    The decorator is used to identify which user function id a phrase function.
    :param:  prio The priority in which it will be executed. Default priority is 100
                  This can be of importance when two or more phrase functions
                  could be triggered.
                  ONLY the phrase function with the highest priority is executed.
    """

    def real_decorator(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            retval = function(*args, **kwargs)
            if len(retval) > 0:
                retval.append(prio)
            return retval
        return wrapper
    return real_decorator
