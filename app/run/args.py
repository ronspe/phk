# -*- coding: utf-8 -*-
import argparse
import app


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("user_scripts_folder",
                        default=app.user.default_folder,
                        help=f'Name of folder to read. Default: "{app.user.default_folder})"'
                        )
    parser.add_argument("-r", "--reload",
                        default='false',
                        help=f'reload userscripts?. Default: false'
                        )
    parser.add_argument("--kill",
                        default='false',
                        help=f'Kills the PHK process'
                        )
    return parser.parse_args()
