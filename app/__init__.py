# -*- coding: utf-8 -*-
"""
Constants that need to be available through the application
"""

import os
import platform
import types
from typing import List, Tuple
from pynput.keyboard import KeyCode

# ini file
ini_file = "config.ini"


# version info
version = types.SimpleNamespace()
version.number = '0.0.4'
version.release_date = '2020-05-18'

# OS info
system = types.SimpleNamespace()
system.os_name = platform.system()
system.os_version = platform.platform()
system.python_version = platform.python_version()
system.phk_run_path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
system.venv_python = os.path.abspath(os.path.join(os.path.dirname(__file__), "..",  ".venv", "bin", "python3"))
system.phk_run_dtm_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "phk", "dtm.py"))
system.phk_pid = None  # Is set dynamically in phk.py

# User scripts specific
user = types.SimpleNamespace()
# FOLDERS:
# This is the root folder where all scripts are stored
user.root_folder = "scripts"
# This folder is always loaded
user.default_folder = "default"
# This will be overwritten by the argument from the commandline
user.context_folder = "default"
# Extension of read Python files
user.file_extension = ".py"
# key reference file
user.key_reference = "key_reference.txt"
# playback speed of phrases in seconds
user.phrase_type_interval = 0.05  # is also set dynamically in phk.py
# playback speed of hotkeys in seconds
user.hotkey_type_interval = 0.05  # is also set dynamically in phk.py

# Macro recorder
recorder = types.SimpleNamespace()
recorder.folder = "default"
recorder.template = "_recorded.template"
recorder.file = "recorded.py"

# Possible actions for a hotkey:
action = types.SimpleNamespace()
action.run = "Run"
action.python = "RunPython"
action.type = "Type"
action.expand = "Expand"  # In a phrase it is more logic to use expand then type
action.return_output = "ReturnOutput"

# Double Tap Menu
dtm = types.SimpleNamespace()
dtm.title = "PHK Double tap menu"
dtm.close = "<Escape>"  # This is a key representation that TKinter uses.
# pickled hotkeys and phrases for the DTM to use
dtm.hotkeys = os.path.abspath(os.path.join(os.path.dirname(__file__), "..",  "hotkeys.pkl"))
dtm.phrases = os.path.abspath(os.path.join(os.path.dirname(__file__), "..",  "phrases.pkl"))



# Data types
ActionEntry = Tuple[str, str, str, str, str]
Actions = List[ActionEntry]
KeyCodes = List[KeyCode]
