# -*- coding: utf-8 -*-
import pytest
from app.phk.recorder import Commands


def test__look_one_command_ahead() -> None:
    l = [0, 1, 2, 3, 4]
    i = 0
    for (this, next_one) in Commands._look_one_command_ahead(l):
        assert this == i
        if next_one is not None:
            assert next_one == i + 1
        i += 1


def test__indent_string() -> None:
    fake_list = [""]
    assert Commands(fake_list)._indent_string("a") == "    a"


def test__get_text_from_command() -> None:
    fake_list = [""]
    c = Commands(fake_list)
    cmd = 'm.keys.type("abc")'
    assert c._get_text_from_command(cmd) == "abc"
    cmd = 'm.keys.press("abc")'
    assert c._get_text_from_command(cmd) == "abc"
    cmd = 'm.keys.type("ABC")'
    assert c._get_text_from_command(cmd) == "ABC"
    cmd = 'm.keys.type(abc)'
    assert c._get_text_from_command(cmd) == ""


def test__group_type_commands_1() -> None:
    l = [
        'm.keys.type("abc")',
        'm.keys.type("def")',
        'm.keys.type("ghi")',
    ]
    c = Commands(l)
    assert c.new_commands == ['    m.keys.type("abcdefghi")']


def test__group_type_commands_2() -> None:
    l = [
        'm.keys.type("abc")',
        'm.keys.press("<enter>")',
        'm.keys.type("def")',
        'm.keys.type("ghi")',
    ]
    c = Commands(l)
    assert c.new_commands == ['    m.keys.type("abc")',
                              '    m.keys.press("<enter>")',
                              '    m.keys.type("defghi")'
                             ]
