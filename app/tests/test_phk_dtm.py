# -*- coding: utf-8 -*-
import pytest
from datetime import datetime, timedelta
from app.phk.keyboard_listener import is_double_tapped


def test_is_double_tapped() -> None:
    # 1 second is too long
    last_timestamp = datetime.now() - timedelta(seconds=1)
    assert is_double_tapped(last_timestamp, max_secs_difference=0.5) is False
    # 0.2 seconds is good
    last_timestamp = datetime.now() - timedelta(seconds=0.2)
    assert is_double_tapped(last_timestamp, max_secs_difference=0.5) is True
