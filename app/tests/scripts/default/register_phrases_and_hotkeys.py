# -*- coding: utf-8 -*-
from app.script import RunPython, Expand, this_file

THIS_FILE = this_file(__file__)


# One hotkey should be loaded
hotkeys = []
hotkeys.append([RunPython, "<ctrl><alt>0", THIS_FILE, "Hello world hotkey"])

# Only two phrases should be loaded
phrases = [
    [Expand, ";kr", "Kind regards,<enter>Ronald", "Will be overwritten by next one"],
    [Expand, ";kr", "Kind regards,<enter>Ronald", "Kind regards"],

    [Expand, ";aaa", "Kind regards,<enter>Ronald", "Kind regards"],

]

# Should not be registered because the trigger is empty
phrases.append([Expand, "", "Kind regards,<enter>Ronald", "Kind regards"])

# Should not be registered because the required fields are not filled
phrases.append([Expand ])
phrases.append([Expand, "aaa" ])

# Should be registered even though the previous had errors
phrases.append([Expand, ";bbb", "test", "description" ])



def main():
    print("Hello world")


if __name__ == "__main__":
    main()
