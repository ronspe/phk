# -*- coding: utf-8 -*-
import pytest
from pynput.keyboard import KeyCode, Key
from app.phk.recorder import MacroListener

stop_keys = "<ctrl><alt>-"


def test_current_active_key() -> None:
    ml = MacroListener(stop_keys)
    # add one key
    ml.current_active_keys.add(Key.shift)
    assert ml.current_active_keys == {Key.shift}
    # and another
    ml.current_active_keys.add(Key.alt)
    result = ml.current_active_keys
    expected = {Key.shift, Key.alt}
    assert all([a == b for a, b in zip(result, expected)])
    # and now a normal character
    normal_char = KeyCode.from_char("a")
    ml.current_active_keys.add(normal_char)
    result = ml.current_active_keys
    expected = {Key.shift, Key.alt, normal_char}
    assert all([a == b for a, b in zip(result, expected)])
    # remove them all
    ml._on_release()
    assert ml.current_active_keys == set()


def test__on_press_typing_single_keys() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True

    # Press: "A"
    ml._on_press(KeyCode.from_char("A"))
    assert len(ml.current_active_keys) == 1
    # current_active_keys are always lower case
    assert ml.current_active_keys == {KeyCode.from_char("a")}
    # Release: "A"
    ml._on_release()
    assert len(ml.current_active_keys) == 0
    assert ml.commands == ['m.keys.type("A")']

    # Press: "b"
    ml._on_press(KeyCode.from_char("b"))
    assert len(ml.current_active_keys) == 1
    assert ml.current_active_keys == {KeyCode.from_char("b")}
    # Release: "b"
    ml._on_release()
    assert len(ml.current_active_keys) == 0
    assert ml.commands == ['m.keys.type("A")', 'm.keys.type("b")']

    # Press: "<space>"
    ml._on_press(Key.space)
    assert len(ml.current_active_keys) == 1
    assert ml.current_active_keys == {Key.space}
    # Release: "<space>"
    ml._on_release()
    assert len(ml.current_active_keys) == 0
    assert ml.commands == ['m.keys.type("A")', 'm.keys.type("b")', 'm.keys.type(" ")']

    # Press: "<F1>"
    ml._on_press(Key.f1)
    assert len(ml.current_active_keys) == 1
    assert ml.current_active_keys == {Key.f1}
    # Release: "<F1>"
    ml._on_release()
    assert len(ml.current_active_keys) == 0
    assert ml.commands == ['m.keys.type("A")', 'm.keys.type("b")'
                           , 'm.keys.type(" ")', 'm.keys.press("<f1>")']


def test__on_press_typing_multiple_keys() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True

    # Press: "<ctrl>"
    ml._on_press(Key.ctrl)
    assert len(ml.current_active_keys) == 1
    assert ml.current_active_keys == {Key.ctrl}

    # Press: "<alt>"
    ml._on_press(Key.alt)
    assert len(ml.current_active_keys) == 2
    result = ml.current_active_keys
    expected = {Key.ctrl, Key.alt}
    assert all([a == b for a, b in zip(result, expected)])

    # Press: "b"
    ml._on_press(KeyCode.from_char("b"))
    assert len(ml.current_active_keys) == 3
    result = ml.current_active_keys
    expected = {Key.ctrl, Key.alt, KeyCode.from_char("b")}
    assert all([a == b for a, b in zip(result, expected)])

    # Release all keys
    ml._on_release()
    assert len(ml.current_active_keys) == 0
    assert 'm.keys.press(' in str(ml.commands)
    assert '<alt>' in str(ml.commands)
    assert '<ctrl>' in str(ml.commands)
    assert 'b' in str(ml.commands)
    # This is not ideal, I cannot predict the precise outcome because the order
    # of the keys can vary. For now, knowing that a key combination is a keys.press
    # and not a keys.type must suffice.
    # TODO: sort them so the order is known?


def test__stop_keys_pressed_1() -> None:
    ml = MacroListener(stop_keys)
    ml.active_listener = True

    assert ml._stop_keys_pressed(Key.ctrl) is False
    assert ml._stop_keys_pressed(Key.alt) is False
    assert ml._stop_keys_pressed(KeyCode.from_char("-")) is True
    ml._on_release()


def test__stop_keys_pressed_2() -> None:
    ml = MacroListener("<shift><cmd>a")
    ml.active_listener = True

    assert ml._stop_keys_pressed(Key.shift) is False
    assert ml._stop_keys_pressed(Key.cmd) is False
    assert ml._stop_keys_pressed(KeyCode.from_char("a")) is True
    ml._on_release()
