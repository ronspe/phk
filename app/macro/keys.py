# -*- coding: utf-8 -*-
import time
from pynput.keyboard import Controller
from app.script.action import type_keys
from app.phk.key_convert import KeyConvert


import app.config as config
log = config.macro_logger


class Keys:
    """Type characters or press multiple keys at the same time"""

    @staticmethod
    def type(text: str, raw: bool=False, interval: float = 0.05) -> None:
        """
        Type a text to the screen including special keys one by one.
        E.g.: "Kind regards,<enter>John Smith<ctrl>s"
        If you would like to press multiple keys at the same wait_for, use press

        :param text: The text to type.
        :param raw: If True the text is typed without converting the codes
                for special keys. e.g.:
                raw=False : "a<tab>b" --> "a     b"
                raw=True  : "a<tab>b" --> "a<tab>b"
                Default is False.
        :param interval: This is the time in seconds between each character being typed
                         Default = 0.05 seconds (typing speed)
                         Set it to 0 for the fastest typing speed
        """
        log.debug(f"Typing string: [{text}]")
        type_keys(text, raw=raw, interval=interval)

    # TODO: if this fails, try to type it.
    @staticmethod
    def press(key_combination: str, pause:float=0.5, times:int=1) -> None:
        """
        Press and release a combination of keys that should be pressed at the same wait_for.
        E.g.: "<ctrl><home>" or "<ctrl>c"
        By default we pause half a second to give the receiving application wait_for to
        resond to our keys.

        :param key_combination: The keys that should be pressed at the same wait_for.
        :param pause: Pause in seconds after keypress. Default = 0.5
        :param times: Amount of times to press the key. Default = 1
        """
        try:
            kc = KeyConvert()
            keys = kc.get_hotkey_keys(key_combination)
            keyboard = Controller()
            for i in range(times):
                for key in keys:
                    #log.debug(f"Press key: [{key}]")
                    keyboard.press(key)
                for key in reversed(keys):
                    #log.debug(f"Release key: [{key}]")
                    keyboard.release(key)
            time.sleep(pause)
            log.debug(f"Press and release: [{key_combination}]")
        except Controller.InvalidKeyException:
            # This happens during recording when keys are typed too fast.
            log.debug(f"Press failed, trying to type: [{key_combination}]")
            type_keys(key_combination)

