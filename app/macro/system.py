# -*- coding: utf-8 -*-
import os
import platform
import pwd

import app.config as config
log = config.macro_logger


class System:
    """
    Contains convenience functions for checking on which machine the script runs.
    """
    def __init__(self) -> None:
        pass

    @staticmethod
    def os_platform() -> str:
        """
        Returns a unified name of the operation system.

        :return: "linux", "mac" , "windows", "cygwin", "solaris" or "unknown"
        """
        if "linux" in platform.system().lower():
            result = "linux"
        elif os.name == 'mac' or platform.system() == 'Darwin':
            result = "mac"
        elif os.name == 'nt' or platform.system() == 'Windows':
            result = "windows"
        elif 'cygwin' in platform.system().lower():
            result = "cygwin"
        elif platform.system() == "SunOS":
            result = "solaris"
        else:
            result = "unknown"
            log.warning(f"OS not recognized: [{os.name}] [{platform.system()}]")
        log.debug(f"OS: [{result}]")
        return result

    @staticmethod
    def os_version() -> str:
        """
        Returns the detailed version of the OS
        """
        version = platform.platform()
        log.debug(f"OS version: [{version}]")
        return version

    @staticmethod
    def python_version() -> str:
        """
        Returns the version number of Python
        """
        version = platform.python_version()
        log.debug(f"Python version: [{version}]")
        return version

    @staticmethod
    def user_name() -> str:
        """
        Get the username from the environment.
        If that fails try the password database.
        WARNING: Do not use this for sensitive scripts because
        the environment variables can be manipulated!
        """
        user = None
        for name in ('LOGNAME', 'USR', 'USER', 'LNAME', 'USERNAME'):
            usr = os.environ.get(name)
            if usr:
                user = usr
                break
        if not user:
            user = pwd.getpwuid(os.getuid())[0]
        if not user:
            log.warning("User name not detected.")
        else:
            log.debug(f"User name: [{user}]")
        return user

    @staticmethod
    def user_homedir() -> str:
        """
        Returns the path to the home directory of the current user.
        """
        homedir = os.path.expanduser("~")
        log.debug(f"User homedir: [{homedir}]")
        return homedir

