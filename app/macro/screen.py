# -*- coding: utf-8 -*-
from pyautogui import size
import mouseinfo

class Screen:
    """
    Get information about the current screen.

    Available values:
      * screen_width   = The width of the screen
      * screen_height  = The height of the screen
      * center_x       = The x position of the center of the screen
      * center_y       = The y position of the center of the screen
      * center         = The (x, y) position of the center of the screen
      * mouse_position = The current position of the mouse

    """

    def __init__(self) -> None:
        self.screen_width, self.screen_height = size()
        self.center_x = round(self.screen_width / 2)
        self.center_y = round(self.screen_height / 2)
        self.center = (self.center_x, self.center_y)
        self.mouse_position = mouseinfo.position()
