# -*- coding: utf-8 -*-
from app.config import macro_logger


class MacroLog:
    """Add logging messages to your macro.log"""

    def __init__(self):
        self.log = macro_logger

    def debug(self, message: str) -> None:
        """
        Log debug messages.
        Debug messages appear only in the macro.log, they do not show on the output.

        :param message: The message in the log.
        """
        self.log.debug(message)

    def info(self, message: str) -> None:
        """
        Log info messages.
        Info messages appear on the output and in the macro.log.

        :param message: The message in the log.
        """
        self.log.info(message)

    def warning(self, message: str) -> None:
        """
        Log warning messages.
        Warning messages appear on the output and in the macro.log.

        :param message: The message in the log.
        """
        self.log.warning(message)

    def error(self, message: str) -> None:
        """
        Log error messages.
        Error messages appear on the output and in the macro.log.

        :param message: The message in the log.
        """
        self.log.error(message)

    def critical(self, message: str) -> None:
        """
        Log critical messages.
        Critical messages appear on the output and in the macro.log.

        :param message: The message in the log.
        """
        self.log.critical(message)
