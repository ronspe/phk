# -*- coding: utf-8 -*-
import app.macro.mouse
from app.macro.clipboard import Clipboard as _Clipboard
# from app.macro.input import Input as _Input
from app.macro.keys import Keys as _Keys
from app.macro.log import MacroLog as _MacroLog
# from app.macro.message import Message as _Message
from app.macro.network import Network as _Network
from app.macro.screen import Screen as _Screen
from app.macro.system import System as _System
from app.macro.shell import Shell as _Shell
from app.macro.wait import Wait as _Wait
from app.macro.user import User as _User
from app.macro import sound as _Sound


clipboard = _Clipboard()
# input = _Input()
keys = _Keys()
log = _MacroLog()
# message = _Message()
network = _Network()
wait = _Wait()
sound = _Sound
screen = _Screen()
system = _System()
shell = _Shell()
user = _User()



# TODO: wait for color change in a pixel
# TODO: collect functionaliteit toevoegen. moet eigen lognaam/path in kunnen vullen
# TODO: example: os, user folder
# TODO: window management
# TODO: killswitch with special key. own thread?
# TODO: clipboard rich text, images, html: https://pypi.org/project/jaraco.clipboard/
# TODO: example: os, user folder
# TODO: example: copy clipboard to pastebin.com and return the url
# TODO: example: move the mouse to prevent the screen from falling asleep
# TODO: ooit: simple form for entering and collectin data, timesheet, todo etc.
