# -*- coding: utf-8 -*-
import time

from pynput.keyboard import KeyCode, Listener
from pynput import mouse
#from pyautogui import position
#from pyautogui.screenshotUtil import pixel
import mouseinfo
from app.phk.key_convert import KeyConvert

import app.config as config
log = config.macro_logger


class Wait:
    """
    Pause the macro until a specific event occurs.
    This is useful for parts of the macro that needs manual input.
    """
    def __init__(self) -> None:
        pass

    @staticmethod
    def seconds(seconds: float=1) -> None:
        """
        Pause the script for X seconds.
        The default is 1 second.

        :param seconds: The amount of seconds to pause.
               - This can be an integer: pause(3)
               - This can be a decimal: pause(1.5)
               - Or without seconds: pause() This defaults to 1 second.
        :return:
        """
        time.sleep(seconds)
        log.debug(f"Wait for {seconds} seconds")

    @staticmethod
    def for_key_press(key_representation: str) -> None:
        """
        Pause the macro until a key is pressed.
        The key can be a single key e.g: "<enter>" or "<tab>" or "a"
        or a combination e.g.: "<ctrl><f7>" or "<cmd>a"

        :param key_representation: A string representing one or more keys.
        """
        KeyWait().is_key_pressed(key_representation)


    @staticmethod
    def for_mouse_click(top_left, bottom_right, button: str="left") -> tuple:
        """
        Pause the macro until a mouse has clicked on a target on the screen.
        The target is a rectangle defined by the top_left and bottom_right
        position on the screen.

        :param top_left: Top left position of the target (x, y)
        :param bottom_right: Bottom right position of the target (x, y)
        :param button: The button that must be clicked. Default is "left"
                       possibilities: [left, right, middle]
        :return: The coordinates of the click (x, y)
        """
        coord = MouseWait().clicked_target(top_left, bottom_right, button)
        return coord

    @staticmethod
    def for_colour_change(x: int, y: int, interval: int=1) -> tuple:
        """
        Pause the macro until the pixel at coordinate (x, y) changes colour.
        Any change in colour will cause the macro to continue.

        :param x: Coordinate of x
        :param y: Coordinate of y
        :param interval: Interval in seconds to check. Default = 1 second
        :return: The current colours in rgb and hex format in a tuple.
                e.g.: ((253, 252, 237), 'fdfced')
        """
        return ColourWait().wait_for_change(x, y, interval=interval)

    @staticmethod
    def for_colour_rgb(x: int, y: int, colour: tuple, interval: int=1) -> bool:
        """
        Pause the macro until the pixel at coordinate (x, y) changes
        to a specific colour.

        :param x: Coordinate of x
        :param y: Coordinate of y
        :param colour: The RGB colour code in a tuple. e.g.: (255, 255, 255)
        :param interval: Interval in seconds to check. Default = 1 second
        :return: True
        """
        return (ColourWait().wait_for_colour_rgb(x, y, colour=colour,
                                                 interval=interval))

    @staticmethod
    def for_colour_hex(x: int, y: int, colour: str, interval: int=1) -> bool:
        """
        Pause the macro until the pixel at coordinate (x, y) changes
        to a specific colour.

        :param x: Coordinate of x
        :param y: Coordinate of y
        :param colour: The hex colour code. e.g.: '#ffffff' or 'ffffff'
        :param interval: Interval in seconds to check. Default = 1 second
        :return: True
        """
        return (ColourWait().wait_for_colour_hex(x, y, colour=colour,
                                                 interval=interval))

class KeyWait:
    def __init__(self) -> None:
        self.current_active_keys = set()
        self.key_found = False
        self.keys = None

    def _on_press(self, key: KeyCode) -> None:
        """
        Check on every key press if the keys we are looking for is pressed.
        If found: the listener is stopped.
        """
        key = KeyConvert().lowercase_char(key)
        if key in self.keys:
            try:
                self.current_active_keys.add(key)
            except KeyError:
                pass
            if all(k in self.current_active_keys for k in self.keys):
                self.key_found = True
                self.listener.stop()

    def _on_release(self, key: KeyCode) -> None:
        """
        Remove the active keys from the set.
        """
        try:
            self.current_active_keys.remove(key)
        except KeyError:
            pass

    def is_key_pressed(self, key_representation: str) -> bool:
        """
        Checks if a key is pressed.
        The key can be a single key e.g: "<enter>" or "<tab>" or "a"
        or a combination e.g.: "<ctrl><f7>" or "<cmd>a"
        :param key_representation: A string representing one or more keys.
        :return: True or False
        """
        self.keys = KeyConvert().get_hotkey_keys(key_representation)
        log.debug(f"Waiting for key: {key_representation}")
        with Listener(
                on_press=self._on_press, on_release=self._on_release
        ) as self.listener:
            self.listener.join()
        if self.key_found:
            log.debug(f"Key: {key_representation} pressed")
        else:
            log.debug(f"Key: {key_representation} not pressed")
        return self.key_found


class MouseWait:
    def __init__(self) -> None:
        self.top_left = None
        self.bottom_right = None
        self.button = None
        self.button_is_clicked: bool = False
        self.coord = None

    def _on_click(self, x: int, y: int, button, pressed: bool) -> None:
        """
        Checks if a click happened in the targeted area.
        If so it stops the listener.
        """
        if pressed:
            if button.name == self.button:
                if self._in_rectangle():
                    log.debug(f'Mouse clicked at: ({x}, {y}) with {button}')
                    self.button_is_clicked = True
                    self.coord = (x, y)
                    self.listener.stop()

    def _on_move(self, x , y):
        pass

    def _on_scroll(self, x, y, dx, dy):
        pass

    def clicked_target(self, top_left, bottom_right, button="left") -> tuple:
        """
        Checkes if a mouse has clicked on a target on the screen.
        The target is a rectanglular area defined by the top_left and bottom_right
        position on the screen.
        :param top_left: Top left position of the target (x, y)
        :param bottom_right: Bottom right position of the target (x, y)
        :param button: The button that must be clicked. Default is "left"
                       possibilities: [left, right, middle]
        :return: The coordinates of the click (x, y)
        """
        log.debug(f"Waiting for mouse click: {top_left}x{bottom_right} {button}")
        self.top_left = top_left
        self.bottom_right = bottom_right
        self.button = button
        with mouse.Listener(on_move=self._on_move,
                            on_click=self._on_click,
                            on_scroll=self._on_scroll
                            ) as self.listener:
            self.listener.join()
        return self.coord

    def _in_rectangle(self) -> bool:
        """
        Checks if the current position is within the rectangle.
        :return: True or False
        """
        x = mouseinfo.position()[0]
        y = mouseinfo.position()[1]
        rect_top = self.top_left[1]
        rect_bottom = self.bottom_right[1]
        rect_left = self.top_left[0]
        rect_right = self.bottom_right[0]
        if (x >= rect_left and x <=rect_right and
                y >= rect_top and y <= rect_bottom):
            return True
        else:
            return False



class ColourWait:
    def __init__(self) -> None:
        pass

    def wait_for_change(self, x: int, y: int, interval: int=1) -> tuple:
        """
        Return the colour when a change in colour at a coordinate occurs.
        :param x: Coordinate of x
        :param y: Coordinate of y
        :param interval: Interval in seconds to check. Default = 1 second
        :return: The current colours in rgb and hex format in a tuple.
        e.g.: ((253, 252, 237), 'fdfced')
        """
        log.debug(f"Waiting for colour change on pixel: ({x}, {y})")
        original_colour_rgb = mouseinfo.getPixel(x, y)
        current_colour_rgb = ()
        is_same = True
        while is_same:
            time.sleep(interval)
            current_colour_rgb = mouseinfo.getPixel(x, y)
            if current_colour_rgb != original_colour_rgb:
                is_same = False
        current_colour_hex = self.rgb_to_hex(current_colour_rgb)
        result = (current_colour_rgb, current_colour_hex)
        return result

    @staticmethod
    def wait_for_colour_rgb(x: int, y: int, colour: tuple,
                            interval: int=1) -> bool:
        """
        Return True when the colour at a coordinate changes to the wanted colour.
        :param x: Coordinate of x
        :param y: Coordinate of y
        :param colour: The RGB colour code in a tuple. e.g.: (255, 255, 255)
        :param interval: Interval in seconds to check. Default = 1 second
        :return:  True
        """
        log.debug(f"Waiting for RGB colour [{colour}] on pixel: ({x}, {y})")
        is_same = True
        while is_same:
            time.sleep(interval)
            colour_rgb = mouseinfo.getPixel(x, y)
            if colour_rgb == colour:
                is_same = False
        return True

    def wait_for_colour_hex(self, x: int, y: int, colour: str,
                            interval: int=1) -> bool:
        """
        Return True when the colour at a coordinate changes to the wanted colour.
        :param x: Coordinate of x
        :param y: Coordinate of y
        :param colour: The hex colour code. e.g.: '#ffffff' or 'ffffff'
        :param interval: Interval in seconds to check. Default = 1 second
        :return: True
        """
        log.debug(f"Waiting for Hex colour [{colour}] on pixel: ({x}, {y})")
        colour = colour.lstrip('#')
        is_same = True
        while is_same:
            time.sleep(interval)
            colour_rgb = mouseinfo.getPixel(x, y)
            colour_hex = self.rgb_to_hex(colour_rgb)
            if colour_hex == colour:
                is_same = False
        return True

    @staticmethod
    def hex_to_rgb(value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

    @staticmethod
    def rgb_to_hex(rgb):
        return '%02x%02x%02x' % rgb
