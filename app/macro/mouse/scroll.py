# -*- coding: utf-8 -*-
from pyautogui import vscroll, hscroll
from app.macro.mouse.log import log_action


class Scroll:

    # TODO: vscroll en hscroll is only available for linux
    @staticmethod
    def up(up: int) -> None:
        """
        Scroll with the mousewheel up.
        :param up: The amount to scroll up
        """
        vscroll(abs(up))
        log_action("scroll-up", up)

    @staticmethod
    def down(down: int) -> None:
        """
        Scroll with the mousewheel down.
        :param down: The amount to scroll down
        """
        negative_clicks = -down
        vscroll(negative_clicks)
        log_action("scroll-down", down)

    @staticmethod
    def right(right: int) -> None:
        """
        Scroll with the mousewheel right.
        :param right: The amount to scroll right
        """
        hscroll(abs(right))
        log_action("scroll-right", right)

    @staticmethod
    def left(left: int) -> None:
        """
        Scroll with the mousewheel left.
        :param left: The amount to scroll left
        """
        negative_clicks = -left
        hscroll(negative_clicks)
        log_action("scroll-left", left)





