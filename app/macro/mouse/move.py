# -*- coding: utf-8 -*-
from pyautogui import moveRel
from app.macro.mouse.log import log_action


class Move:

    @staticmethod
    def relative(offset: tuple, seconds: int=0) -> None:
        """
        Move the mouse relative from the current mouse position. No clicking.
        :param offset: The offset to move. (x, y)
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        """
        moveRel(offset[0], offset[1], seconds)
        log_action("move-relative [{}]".format(offset), 0)

    @staticmethod
    def down(down: int, seconds: int=0) -> None:
        """
        Move the mouse down. No clicking.
        :param down: The amount to move down
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        """
        moveRel(0, abs(down), seconds)
        log_action("move-down [{}]".format(down), 0)

    @staticmethod
    def up(up: int, seconds: int=0) -> None:
        """
        Move the mouse up. No clicking.
        :param up: The amount to move up.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        """
        moveRel(0, -up, seconds)
        log_action("move-up [{}]".format(up), 0)

    @staticmethod
    def left(left: int, seconds: int=0) -> None:
        """
        Move the mouse to the left. No clicking.
        :param left: The amount to move left.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        """
        moveRel(-left, 0, seconds)
        log_action("move-left [{}]".format(left), 0)

    @staticmethod
    def right(right: int, seconds: int=0) -> None:
        """
        Move the mouse to the right. No clicking.
        :param right: The amount to move right.
        :param seconds: The amount of wait_for it should take the mouse to
               move from the current position to desired offset.
               Default is 0 seconds.
        """
        moveRel(abs(right), 0, seconds)
        log_action("move-right [{}]".format(right), 0)

