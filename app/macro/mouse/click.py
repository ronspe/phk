# -*- coding: utf-8 -*-
import time
from pyautogui import click, position
from app.macro.mouse.log import log_action


class Click:

    def left(self, coord: tuple=None, clicks: int=1,
             interval: float=0.25, wait: float=0.5) -> tuple:
        """
        Left click with the mouse on a coordinate.
        :param coord: The coordinate where the mouse should click. (x, y)
               If no coordinate is given the current mouse position is used.
        :param clicks: The amount of clicks, default is one click
        :param interval: If multiple clicks: The interval in seconds between clicks.
               Default is 0,25 seconds.
        :param wait: The amount of time in seconds to wait after the click to give the
               application some  time to react. Default = 0.5 seconds
        :return: The coordinates of the click (x, y)
        """
        if coord is None:
            coord = position()
        click(coord[0], coord[1], clicks=clicks, interval=interval, button='left')
        time.sleep(wait)
        log_action("click-left", clicks)
        return (coord[0], coord[1])

    def right(self, coord: tuple=None, clicks: int=1,
              interval: float=0.25, wait: float=0.5) -> tuple:
        """
        Right click with the mouse on a coordinate.
        :param coord: The coordinate where the mouse should click. (x, y)
               If no coordinate is given the current mouse position is used.
        :param clicks: The amount of clicks, default is one click
        :param interval: If multiple clicks: The interval in seconds between clicks.
               Default is 0,25 seconds.
        :param wait: The amount of time in seconds to wait after the click to give the
               application some  time to react. Default = 0.5 seconds
        :return: The coordinates of the click (x, y)
        """
        if coord is None:
            coord = position()
        click(coord[0], coord[1], clicks=clicks, interval=interval, button='right')
        time.sleep(wait)
        log_action("click-right", clicks)
        return (coord[0], coord[1])

    def middle(self, coord: tuple=None, clicks: int=1,
               interval: float=0.25, wait: float=0.5) -> tuple:
        """
        Middle click with the mouse on a coordinate.
        :param coord: The coordinate where the mouse should click. (x, y)
               If no coordinate is given the current mouse position is used.
        :param clicks: The amount of clicks, default is one click
        :param interval: If multiple clicks: The interval in seconds between clicks.
               Default is 0,25 seconds.
        :param wait: The amount of time in seconds to wait after the click to give the
               application some  time to react. Default = 0.5 seconds
        :return: The coordinates of the click (x, y)
        """
        if coord is None:
            coord = position()
        click(coord[0], coord[1], clicks=clicks, interval=interval, button='middle')
        time.sleep(wait)
        log_action("click-middle", clicks)
        return (coord[0], coord[1])
