# -*- coding: utf-8 -*-
import app.config as config
from pyautogui import position

log = config.macro_logger


def log_action(action: str, clicks: int=0) -> None:
    """Log the actions to a log file."""
    pos = f"[{position()[0]}x{position()[1]}]"
    if clicks > 0:
        message = f"Mouse {action}, clicks: {clicks} {pos}"
    else:
        message = f"Mouse {action}, {pos}"
    log.debug(message)
