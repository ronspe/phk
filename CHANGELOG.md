# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.4] - 2020-05-18
### Added
- The typing speed of phrases and hotkeys is now maintainable in the config.ini

## [0.0.3] - 2020-05-17
### Added
- Phrase functions: Custom text expansion using regular expressions and more.
  See [Documentation](/doc/phrasefunctions.md)


## [0.0.2] - 2020-05-03
### Added
- This CHANGELOG.md
- PHK shows version number and PID when it starts.
- PHK will automatically kill the previous instance of PHK.
  This makes reloading PHK hotkeys and phrases much easier.
- The PHK process can be killed from the commandline with the --kill or -k parameter
  e.g.: ./phk.sh --kill

### Changed
- There is no longer a need for a lockfile

## [0.0.1] - 2020-04-10
This is the initial release
