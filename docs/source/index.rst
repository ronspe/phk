**********************************
PythonHotKey's (PHK) documentation
**********************************

What is this?
*************
| PythonHotKey is a Python application that can be used to emulate keystrokes, move and click the mouse, launch programs, and open documents.
| These actions can be triggered by hotkeys like [F7] or [ctrl + 4] or by a typed phrase.

| PythonHotKey (PHK) tries to be an alternative for AutoHotKey on Linux and MacOS.
| PHK is build on top of the PyAutoGUI and Pynput packages.
| PHK is currently only tested on Linux (Xubuntu) with Python 3.6 and only limited tested on a Macbook Air with Python 3.7.

| The :doc:`PHK macro-scripts <macro_documentation>` can be written in pure Python.
| There is a `macro library <macro_documentation.html#documentation-of-macro-commands>`_ that makes writing the macros very easy.
| There is even a `macro recorder <macro_documentation.html#macro-recorder>`_ that can do most of the work for you.

Current status
**************
| PHK is still very much in development. I have used it daily for a year on my PC (XUbuntu 18.04) with Python 3.6
| I have also did limited testing on MacOS with Python 3.7.

| There is no installer and limited documentation which makes the `installation <installation.html>`_ a bit difficult if you are not familiar with Python.

| To make PHK mature it will need contributers to test and fix issues on different Linux distributions and on MacOS.
| If you are interested to contribute please contact me: ronaldDOTspeelmanATgmailDOTcom

| The current version is 0.0.4
| View the CHANGELOG here: https://gitlab.com/ronspe/phk/-/blob/master/CHANGELOG.md

Code on GitLab
**************
You can find the code here: https://gitlab.com/ronspe/phk


Contents:
*********
.. toctree::
   :maxdepth: 3

   installation
   how_does_it_work
   starting_applications
   phrases
   phrasefunctions
   remembering_triggers
   macro_documentation


